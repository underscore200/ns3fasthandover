#include "ns3/tag.h"

namespace ns3 {

class MCVAPRSSITag: public Tag {
public:
	MCVAPRSSITag ();

	/**
	 * \brief Set the tag's RSSI
	 *
	 * \param rssi the RSSI
	 */
	void SetRSSI(double rssi);
	double GetRSSI(void) const;

	/**
	 * \brief Set the tag's PER
	 *
	 * \param per the PER
	 */
	void SetPER(double per);
	double GetPER(void) const;

	/**
	 * \brief Get the type ID.
	 * \return the object TypeId
	 */
	static TypeId GetTypeId(void);

	// inherited function, no need to doc.
	virtual TypeId GetInstanceTypeId(void) const;

	// inherited function, no need to doc.
	virtual uint32_t GetSerializedSize(void) const;

	// inherited function, no need to doc.
	virtual void Serialize(TagBuffer i) const;

	// inherited function, no need to doc.
	virtual void Deserialize(TagBuffer i);

	// inherited function, no need to doc.
	virtual void Print(std::ostream &os) const;

private:
	double m_rssi; //!< rssi value
	double m_per; //!< packet error rate(PER) value
};
}
