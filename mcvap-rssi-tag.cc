#include "mcvap-rssi-tag.h"
#include "ns3/ptr.h"
#include "ns3/tag.h"
#include "ns3/object.h"

namespace ns3 {
MCVAPRSSITag::MCVAPRSSITag() {
	//NS_LOG_FUNCTION(this);
	m_rssi = 0;
	m_per = 0;
}

void MCVAPRSSITag::SetRSSI(double rssi) {
	m_rssi = rssi;
}

double MCVAPRSSITag::GetRSSI(void) const {
	//NS_LOG_FUNCTION(this);
	return m_rssi;
}

void MCVAPRSSITag::SetPER(double per) {
	m_per = per;
}

double MCVAPRSSITag::GetPER(void) const {
	//NS_LOG_FUNCTION(this);
	return m_per;
}

NS_OBJECT_ENSURE_REGISTERED(MCVAPRSSITag);

TypeId MCVAPRSSITag::GetTypeId(void) {
	static TypeId tid =
			TypeId("ns3::MCVAPRSSITag").SetParent<Tag>().AddConstructor<
					MCVAPRSSITag>();
	return tid;
}

TypeId MCVAPRSSITag::GetInstanceTypeId(void) const {
	return GetTypeId();
}

uint32_t MCVAPRSSITag::GetSerializedSize(void) const {
	//NS_LOG_FUNCTION(this);
	return sizeof(double) + sizeof(double); //!< rssi and per
}
void MCVAPRSSITag::Serialize(TagBuffer i) const {
	//NS_LOG_FUNCTION(this << &i);
	i.WriteDouble(m_rssi);
	i.WriteDouble(m_per);
}
void MCVAPRSSITag::Deserialize(TagBuffer i) {
	//NS_LOG_FUNCTION(this << &i);
	m_rssi = i.ReadDouble();
	m_per = i.ReadDouble();
}
void MCVAPRSSITag::Print(std::ostream &os) const {
	//NS_LOG_FUNCTION(this << &os);
	os << "RSSI=" << m_rssi <<",PER=" << m_per;
}
}
