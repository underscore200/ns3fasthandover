#include "ns3/log.h"
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/bridge-helper.h"
#include "ns3/propagation-module.h"
#include "ns3/gtk-config-store.h"
#include <vector>
#include <stdint.h>
#include <sstream>
#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("ppbpTest");

void sinkReceive(const Ptr<const Packet> packet, const Address &address) {
	static Time revT = Simulator::Now();
	//NS_LOG_LOGIC("Received " << packet->GetSize() << " from " << address);
	Time delta = (Simulator::Now() - revT);
	std::cout << delta.GetMilliSeconds() << std::endl;
	revT = Simulator::Now();
}

int main(int argc, char *argv[]) {

	//int maxChannel = 11;
	uint32_t nWifis = 2;
	bool writeMobility = false;
	double velX = 0.5, velY = 0;//, rssi_th = RSSI_THRESHOLD;
	double startApp = 0.5, endApp = 60.0;

	CommandLine cmd;
	cmd.AddValue("nWifis", "Number of wifi networks", nWifis);
	cmd.AddValue("writeMobility", "Write mobility trace", writeMobility);
	cmd.AddValue("velX", "Horizontal velocity of the station", velX);
	cmd.AddValue("velY", "Vertical velocity of the station", velY);
	cmd.AddValue("startApp", "Start time for application", startApp);
	cmd.AddValue("endApp", "End time for application", endApp);
	cmd.Parse(argc, argv);

	LogComponentEnable("ppbpTest", LOG_LEVEL_INFO);

	NodeContainer backboneNodes;
	NetDeviceContainer backboneDevices;
	std::vector<NodeContainer> staNodes;
	std::vector<NetDeviceContainer> staDevices;
	std::vector<NetDeviceContainer> apDevices;
	std::vector<Ipv4InterfaceContainer> staInterfaces;
	std::vector<Ipv4InterfaceContainer> apInterfaces;

	InternetStackHelper stack;
	CsmaHelper csma;
	Ipv4AddressHelper ip;
	Ptr<CsmaChannel> csmaChannel = CreateObjectWithAttributes<CsmaChannel>(
			"DataRate", StringValue("100Mbps"), "Delay", StringValue("2ms"));
	ip.SetBase("192.168.0.0", "255.255.255.0");

	backboneNodes.Create(nWifis);
	stack.Install(backboneNodes);

	backboneDevices = csma.Install(backboneNodes, csmaChannel);
	NodeContainer serverNode;
	serverNode.Create(1);
	stack.Install(serverNode);
	NetDeviceContainer serverDev = csma.Install(serverNode, csmaChannel);
	Ipv4InterfaceContainer svrInterface;
	svrInterface = ip.Assign(serverDev);
	double wifiX = 10.0, wifiY = -20.0, deltaX = 60, staHeight = 20;

	//!< Wifi default configuration
	std::string phyMode("DsssRate5_5Mbps");
	WifiHelper wifi = WifiHelper::Default();
	wifi.SetStandard(WIFI_PHY_STANDARD_80211b);

	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
	wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO); //!< Support radiotap capture
	YansWifiChannelHelper wifiChannel;
	//!< reference loss must be changed since 802.11b is operating at 2.4GHz
	wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
	wifiChannel.AddPropagationLoss("ns3::LogDistancePropagationLossModel",
			"Exponent", DoubleValue(3.0), //!< office path loss exponent
			"ReferenceLoss", DoubleValue(40.0459));
	wifiPhy.SetChannel(wifiChannel.Create());
	//!< Add a non-QoS upper mac, and disable rate control
	NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();
	wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager", "DataMode",
			StringValue(phyMode), "ControlMode", StringValue(phyMode));
	Ssid staSSID = Ssid("ns3Handoff");

	for (uint32_t i = 0; i < nWifis; ++i) {
		NetDeviceContainer apDev;
		Ipv4InterfaceContainer apInterface;
		MobilityHelper mobility;
		BridgeHelper bridge;

		ListPositionAllocator mLPA;
		mLPA.Add(Vector(wifiX, wifiY, 0));
		mobility.SetPositionAllocator(&mLPA);

		// setup the AP.
		mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
		mobility.Install(backboneNodes.Get(i));
		wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(staSSID)); //!< No SSID is set
		apDev = wifi.Install(wifiPhy, wifiMac, backboneNodes.Get(i));

		NetDeviceContainer bridgeDev;
		bridgeDev = bridge.Install(backboneNodes.Get(i),
				NetDeviceContainer(apDev, backboneDevices.Get(i)));

		// assign AP IP address to bridge, not wifi
		apInterface = ip.Assign(backboneDevices.Get(i));
		apDevices.push_back(apDev);
		apInterfaces.push_back(apInterface);

		// decide new location
		wifiX += deltaX;
	}

	// setup the STA
	NodeContainer sta;
	NetDeviceContainer staDev;
	MobilityHelper mobility;
	Ipv4InterfaceContainer staInterface;
	sta.Create(1);
	stack.Install(sta);
	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	//!< Place the station above the first AP
	ListPositionAllocator mLPA;
	Vector staVec(wifiX - (nWifis * deltaX), wifiY - staHeight, 0);
	mLPA.Add(staVec);
	mobility.SetPositionAllocator(&mLPA);
	// set station velocity
	mobility.Install(sta);

	wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(staSSID),
			"ActiveProbing", BooleanValue(false));
	staDev = wifi.Install(wifiPhy, wifiMac, sta);
	staInterface = ip.Assign(staDev);
	staDevices.push_back(staDev);
	staInterfaces.push_back(staInterface);

	// save everything in containers.
	staNodes.push_back(sta);

	std::string StaServerProtocol = "ns3::UdpSocketFactory";

	Address svrAddress;
	int StaServerPort = 50491;
	svrAddress = InetSocketAddress(svrInterface.GetAddress(0), StaServerPort);
	//OnOffHelper onoff = OnOffHelper(StaServerProtocol, svrAddress);
	PPBPHelper ppbp = PPBPHelper(StaServerProtocol, svrAddress);
	ppbp.SetAttribute("BurstIntensity", StringValue("1Mbps"));
	ppbp.SetAttribute("MeanBurstArrivals",
			StringValue("ns3::ConstantRandomVariable[Constant=5.0]"));
	ApplicationContainer udpApps = ppbp.Install(sta.Get(0)); // added to STA
	udpApps.Start(Seconds(startApp));
	udpApps.Stop(Seconds(endApp));

	// Receiver (Server)
	PacketSinkHelper svrSink(StaServerProtocol,
			InetSocketAddress(Ipv4Address::GetAny(), StaServerPort));
	udpApps = svrSink.Install(serverNode.Get(0)); // added to SERVER
	serverNode.Get(0)->GetApplication(0)->TraceConnectWithoutContext("Rx",
			MakeCallback(&sinkReceive));
	udpApps.Start(Seconds(startApp));
	udpApps.Stop(Seconds(endApp));
	/* Station-server comm end */

	// Calculate Throughput using Flowmonitor
	//monitor = flowmon.InstallAll();

	//wifiPhy.EnablePcap("wifi-bridgingap1", apDevices[0], false);
	//wifiPhy.EnablePcap("wifi-bridgingap2", apDevices[1], false);
	//wifiPhy.EnablePcap("wifi-bridgingsta", staDev, false);
	//csma.EnablePcap("csma-server", serverDev, true);
	//csma.EnablePcap("csmaAp", backboneDevices, true);

	Packet::EnablePrinting();

	Simulator::Stop(Seconds(endApp + 0.5));
	Simulator::Run();
	Simulator::Destroy();
}
