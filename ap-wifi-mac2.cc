/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2006, 2009 INRIA
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 * Author: Mirko Banchi <mk.banchi@gmail.com>
 */

/*
 * Address1 : Destination address
 * Address2 : Source address
 * Address3 : BSSID address
 * */

#include "ap-wifi-mac2.h"
#include <string>

#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/pointer.h"
#include "ns3/boolean.h"
//#include "ns3/integer.h"

#include "qos-tag.h"
#include "wifi-phy.h"
#include "dcf-manager.h"
#include "mac-rx-middle.h"
#include "mac-tx-middle.h"
#include "mgt-headers.h"
#include "mac-low.h"
#include "amsdu-subframe-header.h"
#include "msdu-aggregator.h"
#include "mcvap-rssi-tag.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("ApWifiMac2");

NS_OBJECT_ENSURE_REGISTERED(ApWifiMac2);

Callback<void> cb;

TypeId ApWifiMac2::GetTypeId(void) {
	static TypeId tid =
			TypeId("ns3::ApWifiMac2").SetParent<RegularWifiMac>().AddConstructor<
					ApWifiMac2>().AddAttribute("BeaconInterval",
					"Delay between two beacons",
					TimeValue(MicroSeconds(102400)),
					MakeTimeAccessor(&ApWifiMac2::GetBeaconInterval,
							&ApWifiMac2::SetBeaconInterval), MakeTimeChecker()).AddAttribute(
					"BeaconJitter",
					"A uniform random variable to cause the initial beacon starting time (after simulation time 0) to be distributed between 0 and the BeaconInterval.",
					StringValue("ns3::UniformRandomVariable"),
					MakePointerAccessor(&ApWifiMac2::m_beaconJitter),
					MakePointerChecker<UniformRandomVariable>()).AddAttribute(
					"EnableBeaconJitter",
					"If beacons are enabled, whether to jitter the initial send event.",
					BooleanValue(false),
					MakeBooleanAccessor(&ApWifiMac2::m_enableBeaconJitter),
					MakeBooleanChecker()).AddAttribute("BeaconGeneration",
					"Whether or not beacons are generated.", BooleanValue(true),
					MakeBooleanAccessor(&ApWifiMac2::SetBeaconGeneration,
							&ApWifiMac2::GetBeaconGeneration),
					MakeBooleanChecker());
				 /*.AddAttribute("RssiThreshold",
					"Rssi threshold to trigger for scanning newer APs",
					IntegerValue(-90),
					MakeIntegerAccessor(&ApWifiMac2::SetRssiThres,
							&ApWifiMac2::GetRssiThes),
					MakeIntegerChecker<uint32_t>(-200));
					*/
	return tid;
}

ApWifiMac2::ApWifiMac2() {
	NS_LOG_FUNCTION(this);
	m_beaconDca = CreateObject<DcaTxop>();
	m_beaconDca->SetAifsn(1);
	m_beaconDca->SetMinCw(0);
	m_beaconDca->SetMaxCw(0);
	m_beaconDca->SetLow(m_low);
	m_beaconDca->SetManager(m_dcfManager);
	m_beaconDca->SetTxMiddle(m_txMiddle);

	m_clientManager = new apClientManagero();
	m_watchListManager = new apClientManagero();
	//m_csaAdd = true; // add channel switch annoucement
	// Let the lower layers know that we are acting as an AP.
	SetTypeOfStation(AP);

	m_enableBeaconGeneration = false;
}

ApWifiMac2::~ApWifiMac2() {
	NS_LOG_FUNCTION(this);
}

void ApWifiMac2::DoDispose() {
	NS_LOG_FUNCTION(this);
	m_beaconDca = 0;
	m_enableBeaconGeneration = false;
	m_beaconEvent.Cancel();
	RegularWifiMac::DoDispose();
}

void ApWifiMac2::SetAddress(Mac48Address address) {
	NS_LOG_FUNCTION(this << address);
	// As an AP, our MAC address is also the BSSID. Hence we are
	// overriding this function and setting both in our parent class.
	RegularWifiMac::SetAddress(address);
	RegularWifiMac::SetBssid(address);
}

void ApWifiMac2::SetBeaconGeneration(bool enable) {
	NS_LOG_FUNCTION(this << enable);
	if (!enable) {
		m_beaconEvent.Cancel();
	} else if (enable && !m_enableBeaconGeneration) {
		m_beaconEvent = Simulator::ScheduleNow(&ApWifiMac2::SendOneBeacon,
				this);
	}
	m_enableBeaconGeneration = enable;
}

bool ApWifiMac2::GetBeaconGeneration(void) const {
	NS_LOG_FUNCTION(this);
	return m_enableBeaconGeneration;
}

Time ApWifiMac2::GetBeaconInterval(void) const {
	NS_LOG_FUNCTION(this);
	return m_beaconInterval;
}

void ApWifiMac2::SetWifiRemoteStationManager(
		Ptr<WifiRemoteStationManager> stationManager) {
	NS_LOG_FUNCTION(this << stationManager);
	m_beaconDca->SetWifiRemoteStationManager(stationManager);
	RegularWifiMac::SetWifiRemoteStationManager(stationManager);
}

void ApWifiMac2::SetLinkUpCallback(Callback<void> linkUp) {
	NS_LOG_FUNCTION(this << &linkUp);
	RegularWifiMac::SetLinkUpCallback(linkUp);

	// The approach taken here is that, from the point of view of an AP,
	// the link is always up, so we immediately invoke the callback if
	// one is set
	linkUp();
}

void ApWifiMac2::SetBeaconInterval(Time interval) {
	NS_LOG_FUNCTION(this << interval);
	if ((interval.GetMicroSeconds() % 1024) != 0) {
		NS_LOG_WARN(
				"beacon interval should be multiple of 1024us (802.11 time unit), see IEEE Std. 802.11-2012");
	}
	m_beaconInterval = interval;
}

void ApWifiMac2::StartBeaconing(void) {
	NS_LOG_FUNCTION(this);
	SendOneBeacon();
}

int64_t ApWifiMac2::AssignStreams(int64_t stream) {
	NS_LOG_FUNCTION(this << stream);
	m_beaconJitter->SetStream(stream);
	return 1;
}

void ApWifiMac2::ForwardDown(Ptr<const Packet> packet, Mac48Address from,
		Mac48Address to) {
	NS_LOG_FUNCTION(this << packet << from << to);
	// If we are not a QoS AP then we definitely want to use AC_BE to
	// transmit the packet. A TID of zero will map to AC_BE (through \c
	// QosUtilsMapTidToAc()), so we use that as our default here.
	uint8_t tid = 0;

	// If we are a QoS AP then we attempt to get a TID for this packet
	if (m_qosSupported) {
		tid = QosUtilsGetTidForPacket(packet);
		// Any value greater than 7 is invalid and likely indicates that
		// the packet had no QoS tag, so we revert to zero, which'll
		// mean that AC_BE is used.
		if (tid >= 7) {
			tid = 0;
		}
	}

	ForwardDown(packet, from, to, tid);
}

void ApWifiMac2::ForwardDown(Ptr<const Packet> packet, Mac48Address from,
		Mac48Address to, uint8_t tid) {
	NS_LOG_FUNCTION(
			this << packet << from << to << static_cast<uint32_t> (tid));
	WifiMacHeader hdr;

	// For now, an AP that supports QoS does not support non-QoS
	// associations, and vice versa. In future the AP model should
	// support simultaneously associated QoS and non-QoS STAs, at which
	// point there will need to be per-association QoS state maintained
	// by the association state machine, and consulted here.
	if (m_qosSupported) {
		hdr.SetType(WIFI_MAC_QOSDATA);
		hdr.SetQosAckPolicy(WifiMacHeader::NORMAL_ACK);
		hdr.SetQosNoEosp();
		hdr.SetQosNoAmsdu();
		// Transmission of multiple frames in the same TXOP is not
		// supported for now
		hdr.SetQosTxopLimit(0);
		// Fill in the QoS control field in the MAC header
		hdr.SetQosTid(tid);
	} else {
		hdr.SetTypeData();
	}

	if (m_htSupported) hdr.SetNoOrder();
	hdr.SetAddr1(to);
	hdr.SetAddr2(GetAddress());
	hdr.SetAddr3(from);
	hdr.SetDsFrom();
	hdr.SetDsNotTo();

	if (m_qosSupported) {
		// Sanity check that the TID is valid
		NS_ASSERT(tid < 8);
		m_edca[QosUtilsMapTidToAc(tid)]->Queue(packet, hdr);
	} else {
		m_dca->Queue(packet, hdr);
	}
}

void ApWifiMac2::Enqueue(Ptr<const Packet> packet, Mac48Address to,
		Mac48Address from) {
	NS_LOG_FUNCTION(this << packet << to << from);
	if (to.IsBroadcast() || m_stationManager->IsAssociated(to)) {
		ForwardDown(packet, from, to);
	}
}

void ApWifiMac2::Enqueue(Ptr<const Packet> packet, Mac48Address to) {
	NS_LOG_FUNCTION(this << packet << to);
	// We're sending this packet with a from address that is our own. We
	// get that address from the lower MAC and make use of the
	// from-spoofing Enqueue() method to avoid duplicated code.
	Enqueue(packet, to, m_low->GetAddress());
}

bool ApWifiMac2::SupportsSendFrom(void) const {
	NS_LOG_FUNCTION(this);
	return true;
}

SupportedRates ApWifiMac2::GetSupportedRates(void) const {
	NS_LOG_FUNCTION(this);
	SupportedRates rates;
	// If it is an HT-AP then add the BSSMembershipSelectorSet
	// which only includes 127 for HT now. The standard says that the BSSMembershipSelectorSet
	// must have its MSB set to 1 (must be treated as a Basic Rate)
	// Also the standard mentioned that at leat 1 element should be included in the SupportedRates the rest can be in the ExtendedSupportedRates
	if (m_htSupported) {
		for (uint32_t i = 0; i < m_phy->GetNBssMembershipSelectors(); i++) {
			rates.SetBasicRate(m_phy->GetBssMembershipSelector(i));
		}
	}
	// send the set of supported rates and make sure that we indicate
	// the Basic Rate set in this set of supported rates.
	for (uint32_t i = 0; i < m_phy->GetNModes(); i++) {
		WifiMode mode = m_phy->GetMode(i);
		rates.AddSupportedRate(mode.GetDataRate());
	}
	// set the basic rates
	for (uint32_t j = 0; j < m_stationManager->GetNBasicModes(); j++) {
		WifiMode mode = m_stationManager->GetBasicMode(j);
		rates.SetBasicRate(mode.GetDataRate());
	}

	return rates;
}
HtCapabilities ApWifiMac2::GetHtCapabilities(void) const {
	HtCapabilities capabilities;
	capabilities.SetHtSupported(1);
	capabilities.SetLdpc(m_phy->GetLdpc());
	capabilities.SetShortGuardInterval20(m_phy->GetGuardInterval());
	capabilities.SetGreenfield(m_phy->GetGreenfield());
	for (uint8_t i = 0; i < m_phy->GetNMcs(); i++) {
		capabilities.SetRxMcsBitmask(m_phy->GetMcs(i));
	}
	return capabilities;
}
void ApWifiMac2::SendProbeResp(Mac48Address to) {
	NS_LOG_FUNCTION(this << to);
	std::string prefix = "Client-";
	std::ostringstream smaco;
	smaco << prefix << to;
	Ssid ssid = Ssid(smaco.str());
	WifiMacHeader hdr;
	hdr.SetProbeResp();
	hdr.SetAddr1(to);
	hdr.SetAddr2(GetAddress());
	hdr.SetAddr3(GetAddress());
	hdr.SetDsNotFrom();
	hdr.SetDsNotTo();
	Ptr<Packet> packet = Create<Packet>();
	MgtProbeResponseHeader probe;
	probe.SetSsid(ssid);
	probe.SetSupportedRates(GetSupportedRates());
	probe.SetBeaconIntervalUs(m_beaconInterval.GetMicroSeconds());
	if (m_htSupported) {
		probe.SetHtCapabilities(GetHtCapabilities());
		hdr.SetNoOrder();
	}
	packet->AddHeader(probe);

	// The standard is not clear on the correct queue for management
	// frames if we are a QoS AP. The approach taken here is to always
	// use the DCF for these regardless of whether we have a QoS
	// association or not.
	m_dca->Queue(packet, hdr);
}

void ApWifiMac2::SendAssocResp(Mac48Address to, bool success) {
	NS_LOG_FUNCTION(this << to << success);
	WifiMacHeader hdr;
	hdr.SetAssocResp();
	hdr.SetAddr1(to);
	hdr.SetAddr2(GetAddress());
	hdr.SetAddr3(GetAddress());
	hdr.SetDsNotFrom();
	hdr.SetDsNotTo();
	Ptr<Packet> packet = Create<Packet>();
	MgtAssocResponseHeader assoc;
	StatusCode code;
	if (success) {
		code.SetSuccess();
	} else {
		code.SetFailure();
	}
	assoc.SetSupportedRates(GetSupportedRates());
	assoc.SetStatusCode(code);

	if (m_htSupported) {
		assoc.SetHtCapabilities(GetHtCapabilities());
		hdr.SetNoOrder();
	}
	packet->AddHeader(assoc);

	// The standard is not clear on the correct queue for management
	// frames if we are a QoS AP. The approach taken here is to always
	// use the DCF for these regardless of whether we have a QoS
	// association or not.
	m_dca->Queue(packet, hdr);
}

void ApWifiMac2::SendOneBeacon(void) {
	NS_LOG_FUNCTION(this);
	int counter = 0;
	p_client c = m_clientManager->Get(counter++);
	if (!c && m_beaconEvent.IsRunning()) {
		//std::cout << "no client to beacon" << std::endl;
		m_beaconEvent.Cancel();
	}
	while (c) {
		std::string prefix = "Client-";
		std::ostringstream smaco;
		smaco << prefix << c->m_macaddress;
		Ssid ssid = Ssid(smaco.str());

		WifiMacHeader hdr;
		hdr.SetBeacon();
		hdr.SetAddr1(c->m_macaddress);
		hdr.SetAddr2(GetAddress());
		hdr.SetAddr3(GetAddress());
		hdr.SetDsNotFrom();
		hdr.SetDsNotTo();
		Ptr<Packet> packet = Create<Packet>();
		MgtBeaconHeader beacon;
		beacon.SetSsid(ssid);
		beacon.SetSupportedRates(GetSupportedRates());
		beacon.SetBeaconIntervalUs(m_beaconInterval.GetMicroSeconds());
		if (m_htSupported) {
			beacon.SetHtCapabilities(GetHtCapabilities());
			hdr.SetNoOrder();
		}
		if (c->csa_count > 0) {
			SetCSA(0, c->new_channel, 1);
			beacon.SetCSA(GetCSA());
		}
		packet->AddHeader(beacon);

		// The beacon has it's own special queue, so we load it in there
		m_beaconDca->Queue(packet, hdr);
		if (c->csa_count > 0) {
			NS_LOG_INFO("client " << c->m_macaddress << " is gone");
			m_clientManager->deleteStation(c->m_macaddress);
		}
		c = m_clientManager->Get(counter++);
	}
	m_beaconEvent = Simulator::Schedule(m_beaconInterval,
			&ApWifiMac2::SendOneBeacon, this);
}

void ApWifiMac2::TxOk(const WifiMacHeader &hdr) {
	NS_LOG_FUNCTION(this);
	RegularWifiMac::TxOk(hdr);

	if (hdr.IsAssocResp()
			&& m_stationManager->IsWaitAssocTxOk(hdr.GetAddr1())) {
		NS_LOG_DEBUG("associated with sta=" << hdr.GetAddr1 ());
		m_stationManager->RecordGotAssocTxOk(hdr.GetAddr1());
		m_clientManager->assocStation(hdr.GetAddr1());
		if (!m_beaconEvent.IsRunning()) m_beaconEvent = Simulator::Schedule(
				m_beaconInterval, &ApWifiMac2::SendOneBeacon, this);
	}
}

void ApWifiMac2::TxFailed(const WifiMacHeader &hdr) {
	NS_LOG_FUNCTION(this);
	RegularWifiMac::TxFailed(hdr);

	if (hdr.IsAssocResp()
			&& m_stationManager->IsWaitAssocTxOk(hdr.GetAddr1())) {
		NS_LOG_DEBUG("assoc failed with sta=" << hdr.GetAddr1 ());
		m_stationManager->RecordGotAssocTxFailed(hdr.GetAddr1());
		m_clientManager->deleteStation(hdr.GetAddr1());
	} else if (hdr.IsBeacon()) {
		m_clientManager->deleteStation(hdr.GetAddr1());
	}
}

void ApWifiMac2::RemoveClient(Mac48Address mac) {
	NS_LOG_FUNCTION(this);
	m_clientManager->deleteStation(mac);
	m_stationManager->RecordGotAssocTxFailed(mac);
}

void ApWifiMac2::AddClient(Mac48Address mac) {
	NS_LOG_FUNCTION(this);
	m_clientManager->addStation(mac);
	m_stationManager->RecordGotAssocTxOk(mac);
	if (!m_beaconEvent.IsRunning()) m_beaconEvent = Simulator::Schedule(
			m_beaconInterval, &ApWifiMac2::SendOneBeacon, this);
}

void ApWifiMac2::Receive(Ptr<Packet> packet, const WifiMacHeader *hdr) {
	NS_LOG_FUNCTION(this << packet << hdr);

	Mac48Address from = hdr->GetAddr2();

	if (hdr->IsData()) {
		MCVAPRSSITag tag;
		bool found = packet->PeekPacketTag(tag);
		if (found) {
			double rssi = tag.GetRSSI();
			double per = tag.GetPER();
			if (!m_lowRssi.IsNull()) m_lowRssi(hdr, rssi, per);
			else NS_LOG_WARN("callback for data frame is NULL");
			packet->RemovePacketTag(tag);
		} else NS_FATAL_ERROR("MCVAP Tag not found, please check lower mac"); //!< Change this log level to NS_LOG_WARN
		Mac48Address bssid = hdr->GetAddr1();
		//NS_LOG_INFO("1st:" << (!hdr->IsFromDs()) << " 2nd:" << (hdr->IsToDs()) << " 3rd:"
		//		<< (bssid == GetAddress()) << " 4th:"
		//		<< (m_stationManager->IsAssociated(from)));
		if (!hdr->IsFromDs() && hdr->IsToDs() && bssid == GetAddress()
				&& m_stationManager->IsAssociated(from)) {
			Mac48Address to = hdr->GetAddr3();
			if (to == GetAddress()) {
				NS_LOG_DEBUG("frame for me from=" << from);
				if (hdr->IsQosData()) {
					if (hdr->IsQosAmsdu()) {
						NS_LOG_DEBUG(
								"Received A-MSDU from=" << from << ", size=" << packet->GetSize ());
						DeaggregateAmsduAndForward(packet, hdr);
						packet = 0;
					} else {
						ForwardUp(packet, from, bssid);
					}
				} else {
					ForwardUp(packet, from, bssid);
				}
			} else if (to.IsGroup() || m_stationManager->IsAssociated(to)) {
				NS_LOG_DEBUG("forwarding frame from=" << from << ", to=" << to);
				Ptr<Packet> copy = packet->Copy();

				// If the frame we are forwarding is of type QoS Data,
				// then we need to preserve the UP in the QoS control
				// header...
				if (hdr->IsQosData()) {
					ForwardDown(packet, from, to, hdr->GetQosTid());
				} else {
					ForwardDown(packet, from, to);
				}
				ForwardUp(copy, from, to);
			} else {
				ForwardUp(packet, from, to);
			}
		} else if (hdr->IsFromDs() && hdr->IsToDs()) {
			// this is an AP-to-AP frame
			// we ignore for now.
			NotifyRxDrop(packet);
		} else {
			// we can ignore these frames since
			// they are not targeted at the AP
			NotifyRxDrop(packet);
		}
		return;
	} else if (hdr->IsMgt()) {
		if (hdr->IsProbeReq()) {
			NS_ASSERT(hdr->GetAddr1().IsBroadcast());
			SendProbeResp(from);
			return;
		} else if (hdr->GetAddr1() == GetAddress()) {
			if (hdr->IsAssocReq()) {
				// first, verify that the the station's supported
				// rate set is compatible with our Basic Rate set
				MgtAssocRequestHeader assocReq;
				packet->RemoveHeader(assocReq);
				SupportedRates rates = assocReq.GetSupportedRates();
				bool problem = false;
				for (uint32_t i = 0; i < m_stationManager->GetNBasicModes();
						i++) {
					WifiMode mode = m_stationManager->GetBasicMode(i);
					if (!rates.IsSupportedRate(mode.GetDataRate())) {
						problem = true;
						break;
					}
				}
				if (m_htSupported) { //check that the STA supports all MCSs in Basic MCS Set
					HtCapabilities htcapabilities =
							assocReq.GetHtCapabilities();
					for (uint32_t i = 0; i < m_stationManager->GetNBasicMcs();
							i++) {
						uint8_t mcs = m_stationManager->GetBasicMcs(i);
						if (!htcapabilities.IsSupportedMcs(mcs)) {
							problem = true;
							break;
						}
					}

				}
				if (problem) {
					// one of the Basic Rate set mode is not
					// supported by the station. So, we return an assoc
					// response with an error status.
					SendAssocResp(hdr->GetAddr2(), false);
				} else {
					// station supports all rates in Basic Rate Set.
					// record all its supported modes in its associated WifiRemoteStation
					for (uint32_t j = 0; j < m_phy->GetNModes(); j++) {
						WifiMode mode = m_phy->GetMode(j);
						if (rates.IsSupportedRate(mode.GetDataRate())) {
							m_stationManager->AddSupportedMode(from, mode);
						}
					}
					if (m_htSupported) {
						HtCapabilities htcapabilities =
								assocReq.GetHtCapabilities();
						m_stationManager->AddStationHtCapabilities(from,
								htcapabilities);
						for (uint32_t j = 0; j < m_phy->GetNMcs(); j++) {
							uint8_t mcs = m_phy->GetMcs(j);
							if (htcapabilities.IsSupportedMcs(mcs)) {
								m_stationManager->AddSupportedMcs(from, mcs);
							}
						}
					}
					m_stationManager->RecordWaitAssocTxOk(from);
					// send assoc response with success status.
					SendAssocResp(hdr->GetAddr2(), true);
				}
				return;
			} else if (hdr->IsDisassociation()) {
				m_stationManager->RecordDisassociated(from);
				return;
			}
		}
	}

	// Invoke the receive handler of our parent class to deal with any
	// other frames. Specifically, this will handle Block Ack-related
	// Management Action frames.
	RegularWifiMac::Receive(packet, hdr);
}

void ApWifiMac2::DeaggregateAmsduAndForward(Ptr<Packet> aggregatedPacket,
		const WifiMacHeader *hdr) {
	NS_LOG_FUNCTION(this << aggregatedPacket << hdr);
	MsduAggregator::DeaggregatedMsdus packets = MsduAggregator::Deaggregate(
			aggregatedPacket);

	for (MsduAggregator::DeaggregatedMsdusCI i = packets.begin();
			i != packets.end(); ++i) {
		if ((*i).second.GetDestinationAddr() == GetAddress()) {
			ForwardUp((*i).first, (*i).second.GetSourceAddr(),
					(*i).second.GetDestinationAddr());
		} else {
			Mac48Address from = (*i).second.GetSourceAddr();
			Mac48Address to = (*i).second.GetDestinationAddr();
			NS_LOG_DEBUG("forwarding QoS frame from=" << from << ", to=" << to);
			ForwardDown((*i).first, from, to, hdr->GetQosTid());
		}
	}
}

void ApWifiMac2::setLowRSSICallback(
		Callback<void, const WifiMacHeader *, double, double> cb) {
	m_lowRssi = cb;
}

void ApWifiMac2::DoInitialize(void) {
	NS_LOG_FUNCTION(this);
	m_beaconDca->Initialize();
	m_beaconEvent.Cancel();
	if (m_enableBeaconGeneration) {
		if (m_enableBeaconJitter) {
			int64_t jitter = m_beaconJitter->GetValue(0,
					m_beaconInterval.GetMicroSeconds());
			NS_LOG_DEBUG(
					"Scheduling initial beacon for access point " << GetAddress() << " at time " << jitter << " microseconds");
			m_beaconEvent = Simulator::Schedule(MicroSeconds(jitter),
					&ApWifiMac2::SendOneBeacon, this);
		} else {
			NS_LOG_DEBUG(
					"Scheduling initial beacon for access point " << GetAddress() << " at time 0");
			m_beaconEvent = Simulator::ScheduleNow(&ApWifiMac2::SendOneBeacon,
					this);
		}
	}
	RegularWifiMac::DoInitialize();
}

} // namespace ns3
