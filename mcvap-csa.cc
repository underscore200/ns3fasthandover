#include "mcvap-csa.h"
#include "ns3/assert.h"
#include "ns3/log.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE("MCVAPCSA");

MCVAPCSA::MCVAPCSA() {
	add_csa = false;
	m_csCount = 0;
}

WifiInformationElementId MCVAPCSA::ElementId() const {
	return IE_CHANNEL_SWITCH_ANNOUNCEMENT;
}

void MCVAPCSA::SetCSAMode(uint8_t csaMode) {
	m_csMode = csaMode;
}

void MCVAPCSA::SetCSCount(uint8_t csCount) {
	m_csCount = csCount;
}

void MCVAPCSA::SetNewChannel(uint8_t newChannel) {
	m_newChannel = newChannel;
}

void MCVAPCSA::SetCSA(bool onoff) {
	add_csa = onoff;
}

bool MCVAPCSA::GetCSA() {
	return add_csa;
}

uint8_t MCVAPCSA::GetCSAMode() const {
	return m_csMode;
}

uint8_t MCVAPCSA::GetNewChannel() const {
	return m_newChannel;
}

uint8_t MCVAPCSA::GetCSCount() const {
	return m_csCount;
}

Buffer::Iterator MCVAPCSA::Serialize(Buffer::Iterator i) const {
	if (!add_csa) {
		return i;
	}
	return WifiInformationElement::Serialize(i);
}

uint16_t MCVAPCSA::GetSerializedSize() const {
	if (!add_csa) {
		return 0;
	}
	return WifiInformationElement::GetSerializedSize();
}

uint8_t MCVAPCSA::GetInformationFieldSize () const {
	return 3;
}

void MCVAPCSA::SerializeInformationField(Buffer::Iterator start) const {
	if (add_csa) {
		// write the corresponding value for each bit
		start.WriteU8(GetCSAMode());
		start.WriteU8(GetNewChannel());
		start.WriteU8(GetCSCount());
	}
}

uint8_t MCVAPCSA::DeserializeInformationField(Buffer::Iterator start,
		uint8_t length) {
	Buffer::Iterator i = start;
	uint8_t csMode = i.ReadU8();
	uint8_t csNewCh = i.ReadU8();
	uint8_t csCount = i.ReadU8();
	SetCSAMode(csMode);
	SetNewChannel(csNewCh);
	SetCSCount(csCount);
	return length;
}

ATTRIBUTE_HELPER_CPP(MCVAPCSA);

std::ostream &
operator <<(std::ostream &os, const MCVAPCSA &MCVAPCSA) {
	os << MCVAPCSA.GetCSAMode() << "|" << MCVAPCSA.GetNewChannel() << "|"
			<< MCVAPCSA.GetCSCount();

	return os;
}

std::istream &operator >>(std::istream &is, MCVAPCSA &MCVAPCSA) {
	bool c1, c2, c3;
	is >> c1 >> c2 >> c3;
	MCVAPCSA.SetCSAMode(c1);
	MCVAPCSA.SetNewChannel(c2);
	MCVAPCSA.SetCSCount(c3);

	return is;
}

} // namespace ns3
