/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//
// Default network topology includes some number of AP nodes specified by
// the variable nWifis (defaults to two).  Off of each AP node, there are some
// number of STA nodes specified by the variable nStas (defaults to two).
// Each AP talks to its associated STA nodes.  There are bridge net devices
// on each AP node that bridge the whole thing into one network.
//
//      +-----+
//      | STA |
//      +-----+
//    192.168.0.4  --------------------------->
//      --------
//      WIFI STA
//      --------------------------+
//        ((*))                   |
//                                |
//              ((*))             |             ((*))
//             -------                         -------
//             WIFI AP   CSMA ========= CSMA   WIFI AP
//             -------   ----           ----   -------
//             ##############           ##############
//                 BRIDGE                   BRIDGE
//             ##############           ##############
//               192.168.0.2              192.168.0.3
//               +---------+              +---------+
//               | AP Node |              | AP Node |
//               +---------+--------------+---------+
//                                |
//                          192.168.0.1
//                       +--------------+
//                       |    Server    |
//                       +--------------+
//
#include "ns3/log.h"
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/bridge-helper.h"
#include "ns3/propagation-module.h"
#include "ns3/gtk-config-store.h"
#include <vector>
#include <stdint.h>
#include <sstream>
#include <fstream>

using namespace ns3;

#define AP_SCAN_REQUEST  1
#define AP_SCAN_RESPONSE 2
#define AP_STATION_MOVE  3

#define AP_MSG_PORT 31416
#define RSSI_THRESHOLD (-70)

#define MACADDR_TYPE(x) u_int8_t x[6]
#define MACADDR_TYPE_SZ (sizeof(u_int8_t)*6)
#define f_MACADDR "%02x:%02x:%02x:%02x:%02x:%02x"
#define MACADDR(m) (m)[0], (m)[1], (m)[2], (m)[3], (m)[4], (m)[5]

double global_rssi;
class McvapApp: public Application {
public:

	McvapApp();
	virtual ~McvapApp();

	void Setup(TypeId protocol, Address ListenAddr, uint8_t channel);
	void setLowRssiCallBack(Ptr<ApWifiMac2> m_mac);
	void AddNeighbor(InetSocketAddress);

private:
	virtual void StartApplication(void);
	virtual void StopApplication(void);

	void ScheduleTx(Ptr<Packet> packet, Address dest);
	void SendPacket(Ptr<Packet> packet);
	void HandleAccept(Ptr<Socket> s, const Address& from);
	void HandleRead(Ptr<Socket> s);
	void HandlePeerClose(Ptr<Socket> socket);
	void HandlePeerError(Ptr<Socket> socket);
	void ProcessDataPacket(const WifiMacHeader * hdr, double rssi, double per);
	void ConnectSucc(Ptr<Socket> socket);
	void StartListen();
	void ResetScanRequest(p_client c);
	void ConnectFail(Ptr<Socket> socket);
	void SendRequest(uint8_t code, p_client c);
	void HandleResponse(Ptr<Packet>, Address);
	void ChangeChannel(int channel);

	Ptr<Socket> m_socket; // socket for Sending requests to other APs
	Ptr<Packet> m_packet;
	std::list<Ptr<Socket> > m_socketList; //!< the accepted sockets

	//Address m_peer;
	Address m_local;
	EventId m_sendEvent;
	EventId m_changeChannEv;
	bool m_running;
	uint32_t m_packetsSent;
	Ptr<WifiNetDevice> m_dev;
	TypeId m_tid;
	bool m_connected;
	Ptr<ApWifiMac2> m_APMac;
	apClientManagero * m_clientManager, *m_WatchList;
	uint8_t m_chann; //!< Our channel
	typedef std::vector<InetSocketAddress> neighborList;
	neighborList m_otherAPs; //!< neihgbor APs

	// RSSI threshold
	double m_rssi_thres;
	Time m_response_timeout, m_tcp_interval, m_startTime;
};

NS_LOG_COMPONENT_DEFINE("McvapApp");
McvapApp::McvapApp() :
		m_socket(0), m_sendEvent(), m_packetsSent(0) {
	//m_listen = 0;
	m_running = false;
	m_connected = 0;
	m_clientManager = 0;
	m_rssi_thres = global_rssi; //!< Default RSSI threshold
	m_WatchList = NULL;
	m_chann = 0;

}

McvapApp::~McvapApp() {
	m_socket = 0;
}

void McvapApp::Setup(TypeId protocol, Address ourAddr, uint8_t channel) {
	//m_peer = remoteDest;
	m_local = ourAddr;
	m_tid = protocol;
	m_connected = false;
	m_chann = channel;
	m_response_timeout = Seconds(1.5);
}

void McvapApp::AddNeighbor(InetSocketAddress addr) {
	m_otherAPs.push_back(addr);
}

void McvapApp::StartListen() {
	if (!m_socket) {
		//m_socket->Close(); // needs to be closed before this
		m_socket = Socket::CreateSocket(GetNode(), m_tid);
		m_socket->Bind(InetSocketAddress(Ipv4Address::GetAny(), AP_MSG_PORT));
		m_socket->Listen();
		m_socket->SetRecvCallback(MakeCallback(&McvapApp::HandleRead, this));
		m_socket->SetAcceptCallback(
				MakeNullCallback<bool, Ptr<Socket>, const Address&>(),
				MakeCallback(&McvapApp::HandleAccept, this));
		m_socket->SetCloseCallbacks(
				MakeCallback(&McvapApp::HandlePeerClose, this),
				MakeCallback(&McvapApp::HandlePeerError, this));
	}
	NS_LOG_FUNCTION("lst");
}

void McvapApp::ChangeChannel(int channel) {
	if (!m_dev) m_dev = GetNode()->GetDevice(2)->GetObject<WifiNetDevice>();
	m_dev->GetPhy()->SetChannelNumber(channel);
}

void McvapApp::StartApplication(void) {
	m_running = true;
	std::stringbuf str;
	std::ostream os(&str);
	os << "Starting Application" << " channel " << (int) m_chann;
	NS_LOG_INFO(str.str());

	if (!InetSocketAddress::IsMatchingType(m_local)
			&& !PacketSocketAddress::IsMatchingType(m_local)) {
		NS_LOG_WARN("Local address is not matching type");
	}
	// starttime
	m_startTime = Simulator::Now();
	// start listening on AP_MSG_PORT
	StartListen();

	// Connect to MAC class of the node
	Ptr<WifiNetDevice> wifiAPDevice = GetNode()->GetDevice(2)->GetObject<
			WifiNetDevice>();
	if (!wifiAPDevice) {
		NS_FATAL_ERROR("WifiNetDevice not found");
	}
	Ptr<ApWifiMac2> m_mac = wifiAPDevice->GetMac()->GetObject<ApWifiMac2>();
	if (m_mac) {
		setLowRssiCallBack(m_mac);
	} else {
		NS_FATAL_ERROR("Failed to retrieve ApWifiMac2 for the node");
	}
}

void McvapApp::StopApplication(void) {
	NS_LOG_INFO("Stopping Application");
	m_running = false;

	if (m_sendEvent.IsRunning()) Simulator::Cancel(m_sendEvent);

	if (m_socket) m_socket->Close();

	while (!m_socketList.empty()) { //these are accepted sockets, close them
		Ptr<Socket> acceptedSocket = m_socketList.front();
		m_socketList.pop_front();
		acceptedSocket->Close();
	}
}

void McvapApp::SendPacket(Ptr<Packet> packet) {
	NS_LOG_FUNCTION("Sending Packets");
	if (!m_connected) {
		NS_FATAL_ERROR("No connection, can not send packets");
		return;
	}
	m_socket->Send(packet);
	m_socket->Close();
	StartListen();
}

void McvapApp::ScheduleTx(Ptr<Packet> packet, Address dest) {
	std::stringbuf str;
	std::ostream os(&str);
	os << Simulator::Now() << " Called schedule " << packet->ToString()
			<< " addr = " << InetSocketAddress::ConvertFrom(dest).GetIpv4()
			<< " on port " << InetSocketAddress::ConvertFrom(dest).GetPort();
	NS_LOG_FUNCTION(str.str());
	if (m_running) {
		//if(m_socket)
		//m_socket->Close();
		m_socket = Socket::CreateSocket(GetNode(), m_tid);
		m_socket->Bind(m_local);
		m_socket->Connect(dest);
		m_socket->ShutdownRecv();
		m_socket->SetConnectCallback(MakeCallback(&McvapApp::ConnectSucc, this),
				MakeCallback(&McvapApp::ConnectFail, this));
		m_packet = packet;
	} else {
		NS_LOG_WARN(Simulator::Now() << " Not running");
	}
}

void McvapApp::HandleAccept(Ptr<Socket> s, const Address& from) {
	std::stringbuf str;
	std::ostream os(&str);
	os << Simulator::Now() << "  Connection accepted from "
			<< InetSocketAddress::ConvertFrom(from).GetIpv4() << " port "
			<< InetSocketAddress::ConvertFrom(from).GetPort();
	//NS_LOG_FUNCTION(str.str());
	s->SetRecvCallback(MakeCallback(&McvapApp::HandleRead, this));
	m_socketList.push_back(s);
}

void McvapApp::HandleRead(Ptr<Socket> socket) { //!< Messages from other APs
	Ptr<Packet> packet;
	Address from;
	while ((packet = socket->RecvFrom(from))) {
		if (packet->GetSize() == 0) {
			break; //EOF
		}
		int pSize = packet->GetSize();
		if (InetSocketAddress::IsMatchingType(from)) {
			std::stringbuf str;
			std::ostream os(&str);
			os << "At time " << Simulator::Now().GetSeconds()
					<< "s packet McvapApp received " << pSize << " bytes from "
					<< InetSocketAddress::ConvertFrom(from).GetIpv4()
					<< " port "
					<< InetSocketAddress::ConvertFrom(from).GetPort();
			NS_LOG_FUNCTION(str.str());
		}
		HandleResponse(packet, from);
	}
}

void McvapApp::ResetScanRequest(p_client c) {
	if (c) c->will_handoff = false;
}

void McvapApp::SendRequest(uint8_t code, p_client c) { //!< Includes scan response
	NS_LOG_FUNCTION("SendRequest");
	if (c == NULL) {
		NS_LOG_WARN("NULL pointer for client");
		return;
	}
	std::stringbuf str;
	std::ostream os(&str);

	uint8_t p[64];
	int length = 0;
	p[length++] = code;
	c->m_macaddress.CopyTo(p + length);
	length += 6;
	uint32_t ip = InetSocketAddress::ConvertFrom(m_local).GetIpv4().Get();
	memcpy((p + length), &ip, 4);
	length += 4;
	double rssi;
	switch (code) {
	//!< if it is a scan request send to everyone otherwise send to given
	case AP_SCAN_REQUEST:
		//!< Packet: scanreq code + mac address + ip address + station BSSID + station channel
		c->m_bssid.CopyTo(p + length);
		length += 6;
		p[length++] = m_chann;
		os << Simulator::Now().GetMilliSeconds() << ": Requesting Monitor for "
				<< c->m_macaddress << " current channel " << (uint32_t) m_chann;
		break;
	case AP_SCAN_RESPONSE:
		//!< Packet: scanres code + mac address + ip address + Rssi + our Channel
		if (!m_WatchList) {
			NS_FATAL_ERROR("WatchList is NULL");
		}
		m_WatchList->deleteStation(c->m_macaddress);
		rssi = c->rssi;
		memcpy(p + length, &rssi, sizeof(double));
		length += sizeof(double);
		p[length++] = m_chann;
		os << Simulator::Now().GetMilliSeconds()
				<< ": Sending Monitor Response for " << c->m_macaddress
				<< " new channel " << (uint32_t) m_chann;
		break;
	case AP_STATION_MOVE:
		//!< Packet: stamove code + mac address + ip address + csaCount + beacon Interval
		if (c->new_channel == m_chann || c->new_channel == 0) {
			NS_LOG_INFO("No better AP found: new channel is same channel");
			c->will_handoff = false; //!< set handoff to false for further scanning
			return;
		}
		os << Simulator::Now().GetMilliSeconds() << ": Moving "
				<< c->m_macaddress << " to channel " << c->new_channel;
		p[length++] = (uint8_t) ++(c->csa_count); //!< AP will remove the station
		break;
	default:
		NS_FATAL_ERROR("False code requested: code = " << code);
	}
	// Test packet
	Ptr<Packet> pkt = Create<Packet>(p, length);
	if (m_otherAPs.size() < 1) {
		NS_LOG_WARN("No neighbors");
		return;
	}
	NS_LOG_INFO(str.str());

	if (code == AP_SCAN_REQUEST) { //!< Send to all neighbors and schedule
		for (neighborList::const_iterator i = m_otherAPs.begin();
				i != m_otherAPs.end(); i++) {
			if (InetSocketAddress::ConvertFrom(*i).GetIpv4()
					== InetSocketAddress::ConvertFrom(m_local).GetIpv4()) continue; //!< Dont'send to self
			ScheduleTx(pkt, *i);
		}
		//!< Schedule client move send
		Simulator::Schedule(m_response_timeout, &McvapApp::SendRequest, this,
		AP_STATION_MOVE, c);
		return;
	}
	InetSocketAddress dest = InetSocketAddress(c->m_apIpaddress, AP_MSG_PORT);
	ScheduleTx(pkt, dest);
}

void McvapApp::HandleResponse(Ptr<Packet> packet, Address from) {

	// getCode
	uint8_t code;
	uint8_t mac[6];
	uint32_t ip;
	uint8_t channel;
	packet->CopyData(&code, 1);
	packet->RemoveAtStart(1);
	packet->CopyData(mac, 6);
	packet->RemoveAtStart(6);
	packet->CopyData((uint8_t*) &ip, 4);
	packet->RemoveAtStart(4);

	char t[18];
	sprintf((char *) t, f_MACADDR, MACADDR(mac));
	const char macString[18] = { t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7],
			t[8], t[9], t[10], t[11], t[12], t[13], t[14], t[15], t[16], t[17] };
	Mac48Address macAddr(macString);
	p_client wc;
	Ipv4Address senderIp = Ipv4Address(ip);

	p_client c = NULL;
	uint8_t rssi[8];
	double rssi2;
	if (sizeof(double) != 8)
	NS_FATAL_ERROR("sizeof double is not eight");
	std::stringbuf str;
	std::ostream os(&str);
	uint8_t resp_chann; //!< Channel number of response msg

	switch (code) {
	case AP_SCAN_REQUEST: //!< add to watchlist and (dont)register "from" Address to that client
		os << Simulator::Now().GetMilliSeconds() << ": Scan Requested for :"
				<< macAddr << ", from:" << senderIp;
		packet->RemoveAtStart(6); // BSSID
		packet->CopyData(&channel, 1);
		wc = m_WatchList->addStation(macAddr);
		wc->new_channel = channel;
		wc->m_apIpaddress = senderIp;
		wc->rssi = 0;
		ChangeChannel(channel);
		m_changeChannEv = Simulator::Schedule(Time("50ms"),
				&McvapApp::ChangeChannel, this, m_chann);
		break;
	case AP_SCAN_RESPONSE: //!< look up for given mac from own stations and check for channel switch
		c = m_clientManager->searchStation(macAddr);
		if (!c) {
			NS_LOG_WARN("Scan response for unknown client " << macAddr);
			return;
		}
		packet->CopyData(rssi, 8);
		packet->RemoveAtStart(8);
		packet->CopyData(&resp_chann, 1);
		packet->RemoveAtStart(1);
		memcpy(&rssi2, rssi, 8);
		os << Simulator::Now().GetMilliSeconds()
				<< ": Scan Response with info :" << macAddr << ", ip:"
				<< senderIp << "  comparing RSSI ours:" << c->rssi
				<< " and response:" << rssi2;
		if (rssi2 > c->rssi && rssi2 > c->new_rssi) { //!< dont change the csa count (client move will handle it)
			NS_LOG_INFO("Client " << c->m_macaddress << " will handoff");
			c->new_rssi = rssi2;
			c->m_apIpaddress = senderIp;
			c->new_channel = resp_chann;
		}
		break;
	case AP_STATION_MOVE:
		os << Simulator::Now().GetMilliSeconds() << ": Station Move for :"
				<< macAddr << ", from:" << Ipv4Address(ip);
		//!< Add client to the associated list
		m_APMac->AddClient(macAddr);
		break;
	}
	NS_LOG_INFO(str.str());
}

void McvapApp::ProcessDataPacket(const WifiMacHeader * hdr, double rssi,
		double per) {
	/* BSSID, Source ADDR, Destionation ADDR*/
	std::stringbuf str;
	std::ostream os(&str);
	os << "packet addr1 " << hdr->GetAddr1() << " addr2 " << hdr->GetAddr2()
			<< " addr3 " << hdr->GetAddr3() << " rssi = " << rssi << ", per = "
			<< per;
	NS_LOG_LOGIC(str.str());
	if (per > 0) NS_LOG_INFO("per = " << per);

	Mac48Address bs = hdr->GetAddr1();
	Mac48Address sa = hdr->GetAddr2();
	static int watchCount = 0;
	//Mac48Address da = hdr->GetAddr3();

	if (!m_clientManager || !m_WatchList)
	NS_FATAL_ERROR("Either one of the Client Managers is NULL");

	p_client c = m_clientManager->searchStation(sa);
	p_client wc = m_WatchList->searchStation(sa);

	double before, after;
	if (c != NULL && bs == m_APMac->GetAddress()) { //!< If it is our client (check the RSSI value)
		NS_LOG_LOGIC("Our client " << sa << ", rssi Thres = " << m_rssi_thres);
		c->rssi = rssi;
		if (rssi < m_rssi_thres && !c->will_handoff) {
			NS_LOG_FUNCTION(
					Simulator::Now() << " Rssi(" << rssi << ") dropped for client " << sa);
			if(Simulator::Now() < (m_startTime + Seconds(2.))) {
				NS_LOG_FUNCTION("Too early to handoff");
				return;
			}
			c->will_handoff = true;
			//!< send SCAN request
			SendRequest(AP_SCAN_REQUEST, c);
		}
	} else if (wc != NULL) { //!< watched client (update RSSI and PER)
		watchCount++;
		before = wc->rssi;
		wc->rssi = (wc->rssi * (watchCount - 1) + rssi) / watchCount; //!< average
		after = wc->rssi;
		NS_LOG_FUNCTION(
				Simulator::Now() << " Got rssi for watched client: " << rssi << ", wcount = " << watchCount);
		NS_LOG_FUNCTION("b4: wc->rssi = " << before);
		NS_LOG_FUNCTION("after: wc->rssi = " << after);

		if (watchCount >= 2) { //!< Listen to 2 packets
			watchCount = 0;
			//!< Cancel change channel back event (do it Now) Send Scan Response
			if (m_changeChannEv.IsRunning()) {
				m_changeChannEv.Cancel();
				ChangeChannel(m_chann);
			}
			SendRequest(AP_SCAN_RESPONSE, wc); //!< Send scan response
		}
	}
}

void McvapApp::setLowRssiCallBack(Ptr<ApWifiMac2> m_mac) {
	m_APMac = m_mac;
	m_clientManager = m_APMac->m_clientManager;
	m_WatchList = m_APMac->m_watchListManager;
	for (stationList::iterator i = m_clientManager->m_stations.begin();
			i != m_clientManager->m_stations.end(); i++) {
		((p_client) *i)->m_apIpaddress =
				InetSocketAddress::ConvertFrom(m_local).GetIpv4();
		((p_client) *i)->channel = m_chann;
		((p_client) *i)->new_channel = m_chann;
	}
	m_APMac->setLowRSSICallback(
			MakeCallback(&McvapApp::ProcessDataPacket, this));
}

void McvapApp::ConnectSucc(Ptr<Socket> socket) {
	//std::ostringstream stream;
	//stream <<
	//NS_LOG_FUNCTION(Simulator::Now() << " Connection successfull");
	m_connected = true;
	m_sendEvent = Simulator::Schedule(Time("1ms"), &McvapApp::SendPacket, this,
			m_packet);
}

void McvapApp::ConnectFail(Ptr<Socket> socket) {
	//std::ostringstream stream;
	//stream << "Connection failed to "
	//		<< InetSocketAddress::ConvertFrom(m_local).GetIpv4() << " port "
	//		<< InetSocketAddress::ConvertFrom(m_local).GetPort();
	NS_LOG_WARN(Simulator::Now() << " Failed");
	m_connected = false;
}

void McvapApp::HandlePeerClose(Ptr<Socket> socket) {
	//NS_LOG_FUNCTION("Peer Closed Normally");
}

void McvapApp::HandlePeerError(Ptr<Socket> socket) {
	//NS_LOG_FUNCTION("Peer closed abnormally");
}

FlowMonitorHelper flowmon;
Ptr<FlowMonitor> monitor = NULL;
void startMon() {
	if (!monitor) return;
	monitor->CheckForLostPackets();

	Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier>(
			flowmon.GetClassifier());
	std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats();
	for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i =
			stats.begin(); i != stats.end(); ++i) {
		Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);
		if ((t.sourceAddress == "192.168.0.4"
				&& t.destinationAddress == "192.168.0.1")) {
			//std::cout << "Flow " << i->first << " (" << t.sourceAddress
			//		<< " -> " << t.destinationAddress << ")\n";
			//std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
			//std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
			std::cout << Simulator::Now().GetMilliSeconds() << " Throughput: "
					<< i->second.rxBytes * 8.0
							/ (i->second.timeLastRxPacket.GetSeconds()
									- i->second.timeFirstTxPacket.GetSeconds())
							/ 1024 << " Kbps\n";
		}
	}

	Simulator::Schedule(Seconds(1), &startMon);
}

Ptr<WifiNetDevice> mwifiDev;
void LinkUp() {
	bool newAP = false;
	static bool stationIsUp = true;
	static int apCount = 0;
	static Mac48Address cBssid = Mac48Address();
	if (mwifiDev) {
		stationIsUp = mwifiDev->IsLinkUp();
		Ptr<WifiMac> mac = mwifiDev->GetMac();
		Ptr<RegularWifiMac> rmac = mac->GetObject<RegularWifiMac>();
		if (rmac && rmac->GetBssid() != cBssid) {
			cBssid = rmac->GetBssid();
			newAP = true;
			apCount++;
		}
	} else {
		stationIsUp = !stationIsUp;
	}
	Time t_now = Simulator::Now();
	double f_milli = 1.0e-6 * t_now.GetNanoSeconds();
	NS_LOG_FUNCTION(
			"station is " << ((stationIsUp) ? "UP" : "DOWN") << " with " << ((newAP) ? "new" : "old") << " AP at " << f_milli);
	if (newAP) {
		monitor->StopRightNow();
		monitor->StartRightNow();
		startMon();
	}
}

void sinkReceive(const Ptr<const Packet> packet, const Address &address) {
	static Time revT = Simulator::Now();
	NS_LOG_LOGIC("Received " << packet->GetSize() << " from " << address);
	Time delta = (Simulator::Now() - revT);
	std::cout << delta.GetMilliSeconds() << std::endl;
	revT = Simulator::Now();
}

void changeDirection(Ptr<ConstantVelocityMobilityModel> mob, Time next,
		double velX) {
	mob->SetVelocity(Vector(-velX, 0, 0));
	Simulator::Schedule(next, &changeDirection, mob, next, -velX);
}

int main(int argc, char *argv[]) {

	int maxChannel = 11;
	uint32_t nWifis = 2;
	bool writeMobility = false, outputIAT = false, outputTHRPT = false;
	double velX = 0.5, velY = 0, rssi_th = RSSI_THRESHOLD;
	double startApp = 0.5, endApp = 60.0;

	CommandLine cmd;
	cmd.AddValue("nWifis", "Number of wifi networks", nWifis);
	cmd.AddValue("writeMobility", "Write mobility trace", writeMobility);
	cmd.AddValue("outputIAT", "Output IAT time values to stdout", outputIAT);
	cmd.AddValue("outputTHRPT", "Output throughput values to stdout",
			outputTHRPT);
	cmd.AddValue("velX", "Horizontal velocity of the station", velX);
	cmd.AddValue("velY", "Vertical velocity of the station", velY);
	cmd.AddValue("rssi_th", "Rssi Threshold for the APs", rssi_th);
	cmd.AddValue("startApp", "Start time for application", startApp);
	cmd.AddValue("endApp", "End time for application", endApp);
	cmd.Parse(argc, argv);

	global_rssi = rssi_th;
	LogComponentEnable("McvapApp", LOG_LEVEL_FUNCTION);
	//LogComponentEnable("ApWifiMac2", LOG_LEVEL_FUNCTION);
	//LogComponentEnable("StaWifiMac2", LOG_LEVEL_INFO);
	//LogComponentEnable("YansWifiPhy", LOG_LEVEL_DEBUG);

	NodeContainer backboneNodes;
	NetDeviceContainer backboneDevices;
	//Ipv4InterfaceContainer backboneInterfaces;
	std::vector<NodeContainer> staNodes;
	std::vector<NetDeviceContainer> staDevices;
	std::vector<NetDeviceContainer> apDevices;
	std::vector<Ipv4InterfaceContainer> staInterfaces;
	std::vector<Ipv4InterfaceContainer> apInterfaces;

	InternetStackHelper stack;
	CsmaHelper csma;
	Ipv4AddressHelper ip;
	Ptr<CsmaChannel> csmaChannel = CreateObjectWithAttributes<CsmaChannel>(
			"DataRate", StringValue("100Mbps"), "Delay", StringValue("2ms"));
	ip.SetBase("192.168.0.0", "255.255.255.0");

	backboneNodes.Create(nWifis);
	stack.Install(backboneNodes);

	backboneDevices = csma.Install(backboneNodes, csmaChannel);
	NodeContainer serverNode;
	serverNode.Create(1);
	stack.Install(serverNode);
	NetDeviceContainer serverDev = csma.Install(serverNode, csmaChannel);
	Ipv4InterfaceContainer svrInterface;
	svrInterface = ip.Assign(serverDev);
	double wifiX = 10.0, wifiY = -20.0, deltaX = 60, staHeight = 30;

	//!< Wifi default configuration
	std::string phyMode("DsssRate5_5Mbps");
	WifiHelper wifi = WifiHelper::Default();
	wifi.SetStandard(WIFI_PHY_STANDARD_80211b);

	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
	wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO); //!< Support radiotap capture
	YansWifiChannelHelper wifiChannel;
	//!< reference loss must be changed since 802.11b is operating at 2.4GHz
	wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
	wifiChannel.AddPropagationLoss("ns3::LogDistancePropagationLossModel",
			"Exponent", DoubleValue(3.0), //!< office path loss exponent
			"ReferenceLoss", DoubleValue(40.0459));
	wifiPhy.SetChannel(wifiChannel.Create());
	//!< Add a non-QoS upper mac, and disable rate control
	NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();
	wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager", "DataMode",
			StringValue(phyMode), "ControlMode", StringValue(phyMode));

	for (uint32_t i = 0; i < nWifis; ++i) {
		NetDeviceContainer apDev;
		Ipv4InterfaceContainer apInterface;
		MobilityHelper mobility;
		BridgeHelper bridge;

		ListPositionAllocator mLPA;
		mLPA.Add(Vector(wifiX, wifiY, 0));
		mobility.SetPositionAllocator(&mLPA);

		// setup the AP.
		mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
		mobility.Install(backboneNodes.Get(i));
		wifiMac.SetType("ns3::ApWifiMac2"); //!< No SSID is set
		apDev = wifi.Install(wifiPhy, wifiMac, backboneNodes.Get(i));

		// Change channel of the next AP
		int FreqChannel = 1 + i * ((maxChannel + 1) / nWifis);
		if (i != 0) apDev.Get(0)->GetObject<WifiNetDevice>()->GetPhy()->SetChannelNumber(
				FreqChannel);
		NetDeviceContainer bridgeDev;
		bridgeDev = bridge.Install(backboneNodes.Get(i),
				NetDeviceContainer(apDev, backboneDevices.Get(i)));

		// assign AP IP address to bridge, not wifi
		apInterface = ip.Assign(backboneDevices.Get(i));
		apDevices.push_back(apDev);
		apInterfaces.push_back(apInterface);

		// decide new location
		wifiX += deltaX;
	}

	// setup the STA
	NodeContainer sta;
	NetDeviceContainer staDev;
	MobilityHelper mobility;
	Ipv4InterfaceContainer staInterface;
	sta.Create(1);
	stack.Install(sta);
	mobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
	//!< Place the station above the first AP
	ListPositionAllocator mLPA;
	Vector staVec(wifiX - (nWifis * deltaX), wifiY - staHeight, 0);
	mLPA.Add(staVec);
	mobility.SetPositionAllocator(&mLPA);
	// set station velocity
	mobility.Install(sta);
	Ptr<ConstantVelocityMobilityModel> Stamobility;
	Stamobility = sta.Get(0)->GetObject<ConstantVelocityMobilityModel>();
	Stamobility->SetVelocity(Vector(velX, velY, 0));
	Simulator::Schedule(Seconds(deltaX / velX), &changeDirection, Stamobility,
			Seconds(deltaX / velX), velX);

	Mac48Address staMacAddress;
	Ptr<StaWifiMac2> staMacLayer;
	wifiMac.SetType("ns3::StaWifiMac2", "ActiveProbing", BooleanValue(true)); // "Ssid", SsidValue(staSSID));
	staDev = wifi.Install(wifiPhy, wifiMac, sta);
	Ptr<WifiNetDevice> staNetDevice = staDev.Get(0)->GetObject<WifiNetDevice>();
	if (staNetDevice) staMacLayer = staNetDevice->GetMac()->GetObject<
			StaWifiMac2>();
	else NS_FATAL_ERROR("WifiNetDevice is NULL");
	if (staMacLayer) staMacAddress = staMacLayer->GetAddress();
	else NS_FATAL_ERROR("WifiMac is NULL");

	std::stringbuf str;
	std::ostream os(&str);
	os << "Client-" << staMacAddress;
	Ssid staSSID = Ssid(str.str());
	staMacLayer->SetSsid(staSSID);
	staInterface = ip.Assign(staDev);
	/*
	 Ptr<WifiNetDevice> wifiNetDevice =
	 staDev.Get(0)->GetObject<WifiNetDevice>();
	 mwifiDev = wifiNetDevice;
	 if (wifiNetDevice) wifiNetDevice->AddLinkChangeCallback(
	 MakeCallback(&LinkUp));
	 else NS_FATAL_ERROR("WifiNetDevice is NULL");
	 */
	staDevices.push_back(staDev);
	staInterfaces.push_back(staInterface);

	// save everything in containers.
	staNodes.push_back(sta);

	/*
	// Setup 2nd STA
	NodeContainer sta2;
	NetDeviceContainer staDev2;
	MobilityHelper mobility2;
	Ipv4InterfaceContainer staInterface2;
	sta2.Create(1);
	stack.Install(sta2);
	mobility2.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	ListPositionAllocator mLPA2;
	mLPA2.Add(
			Vector(wifiX - (nWifis * deltaX) + deltaX, wifiY - staHeight - 5,
					0));
	mobility2.SetPositionAllocator(&mLPA2);
	// set station velocity
	mobility2.Install(sta2);
	Ssid staSSID2 = Ssid("Client-00:00:00:00:00:07");
	wifiMac.SetType("ns3::StaWifiMac2", "Ssid", SsidValue(staSSID2),
			"ActiveProbing", BooleanValue(true));
	staDev2 = wifi.Install(wifiPhy, wifiMac, sta2);
	staInterface2 = ip.Assign(staDev2);
	Ptr<WifiNetDevice> wifiNetDevice2 =
			staDev.Get(0)->GetObject<WifiNetDevice>();
	wifiNetDevice2->GetPhy()->SetChannelNumber(7);

	staDevices.push_back(staDev2);
	staInterfaces.push_back(staInterface2);
	staNodes.push_back(sta2);
	// */
	//std::string InterAPProtocol = "ns3::TcpSocketFactory";
	std::string StaServerProtocol = "ns3::UdpSocketFactory";

	/* Inter AP Messages */
	int localPort = 12329;
	for (unsigned int i = 0; i < nWifis; i++) {
		InetSocketAddress addr1 = InetSocketAddress(
				apInterfaces[i].GetAddress(0), localPort);
		Ptr<McvapApp> mcvapp = CreateObject<McvapApp>();
		for (uint32_t j = 0; j < apInterfaces.size(); j++)
			mcvapp->AddNeighbor(InetSocketAddress(apInterfaces[j].GetAddress(0),
			AP_MSG_PORT));
		int channel = 1 + i * ((maxChannel + 1) / nWifis);
		mcvapp->Setup(TcpSocketFactory::GetTypeId(), addr1, channel);
		backboneNodes.Get(i)->AddApplication(mcvapp); // added to AP1
		mcvapp->SetStartTime(Seconds(startApp));
		mcvapp->SetStopTime(Seconds(endApp));
	}
	/* Station-Server comm start */ //!< VOIP G.711
	// sender (station)
	Address svrAddress;
	int StaServerPort = 50491;
	svrAddress = InetSocketAddress(svrInterface.GetAddress(0), StaServerPort);
	OnOffHelper onoff = OnOffHelper(StaServerProtocol, svrAddress);
	onoff.SetConstantRate(DataRate("65kb/s"));
	//onoff.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
	//onoff.SetAttribute("OffTime",StringValue("ns3::ConstantRandomVariable[Constant=0]"));
	onoff.SetAttribute("PacketSize", UintegerValue(172)); //!< Voice payload size of G.711	+ RTP header(12 bytes)

	ApplicationContainer udpApps = onoff.Install(sta.Get(0)); // added to STA
	udpApps.Start(Seconds(startApp));
	udpApps.Stop(Seconds(endApp));
	//udpApps = onoff.Install(sta2.Get(0)); // added to STA2
	//udpApps.Start(Seconds(startApp));
	//udpApps.Stop(Seconds(endApp));

	// Receiver (Server)
	PacketSinkHelper svrSink(StaServerProtocol,
			InetSocketAddress(Ipv4Address::GetAny(), StaServerPort));
	udpApps = svrSink.Install(serverNode.Get(0)); // added to SERVER
	//!< Output Inter-Arrival-Time for packets for server node
	if (outputIAT) serverNode.Get(0)->GetApplication(0)->TraceConnectWithoutContext(
			"Rx", MakeCallback(&sinkReceive));
	udpApps.Start(Seconds(startApp));
	udpApps.Stop(Seconds(endApp));
	/* Station-server comm end */

	// Calculate Throughput using Flowmonitor
	monitor = flowmon.InstallAll();
	//!< Output throughtput for observed node
	if (outputTHRPT) startMon();

	wifiPhy.EnablePcap("wifi-bridgingap1", apDevices[0], false);
	wifiPhy.EnablePcap("wifi-bridgingap2", apDevices[1], false);
	wifiPhy.EnablePcap("wifi-bridgingsta", staDev, false);
	//wifiPhy.EnablePcap("wifi-bridgingsta2", staDev2);
	csma.EnablePcap("csma-server", serverDev, true);
	csma.EnablePcap("csmaAp", backboneDevices, true);

	Packet::EnablePrinting();

	if (writeMobility) {
		AsciiTraceHelper ascii;
		MobilityHelper::EnableAsciiAll(
				ascii.CreateFileStream("wifi-wired-bridging.mob"));
	}

	//GtkConfigStore config;
	//config.ConfigureAttributes();
	Simulator::Stop(Seconds(70.0));
	Simulator::Run();
	Simulator::Destroy();
}
