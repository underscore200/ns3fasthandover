/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/log.h"
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/bridge-helper.h"
#include "ns3/propagation-module.h"
#include "ns3/gtk-config-store.h"
#include "ns3/netanim-module.h"
#include <vector>
#include <stdint.h>
#include <sstream>
#include <fstream>

using namespace ns3;

#define AP_SCAN_REQUEST  1
#define AP_SCAN_RESPONSE 2
#define AP_STATION_MOVE  3

#define AP_MSG_PORT 31416
#define RSSI_THRESHOLD (-70)

#define MACADDR_TYPE(x) u_int8_t x[6]
#define MACADDR_TYPE_SZ (sizeof(u_int8_t)*6)
#define f_MACADDR "%02x:%02x:%02x:%02x:%02x:%02x"
#define MACADDR(m) (m)[0], (m)[1], (m)[2], (m)[3], (m)[4], (m)[5]
#define W_TH(P,N) ((1.0-P)/(1.0*N))

double global_rssi, global_algo;
class McvapApp: public Application {
public:

	McvapApp();
	virtual ~McvapApp();

	void Setup(TypeId protocol, Address ListenAddr, uint8_t channel);
	void setLowRssiCallBack(Ptr<ApWifiMac2> m_mac);
	void AddNeighbor(InetSocketAddress);

private:
	virtual void StartApplication(void);
	virtual void StopApplication(void);

	void ScheduleTx(Ptr<Packet> packet, Address dest);
	void SendPacket(Ptr<Packet> packet);
	void DataSent(Ptr<Socket> packet, uint32_t size);
	void HandleAccept(Ptr<Socket> s, const Address& from);
	void HandleRead(Ptr<Socket> s);
	void HandlePeerClose(Ptr<Socket> socket);
	void HandlePeerError(Ptr<Socket> socket);
	void ProcessDataPacket(const WifiMacHeader * hdr, double rssi, double per);
	void ConnectSucc(Ptr<Socket> socket);
	void StartListen();
	void ResetScanRequest(p_client c);
	void ConnectFail(Ptr<Socket> socket);
	void SendRequest(uint8_t code, p_client c);
	void HandleResponse(Ptr<Packet>, Address);
	void ChangeChannel(int channel);

	Ptr<Socket> m_socket; // socket for Sending requests to other APs
	Ptr<Packet> m_packet;
	std::list<Ptr<Socket> > m_socketList; //!< the accepted sockets

	//Address m_peer;
	Address m_local;
	EventId m_sendEvent, m_changeChannEv;
	bool m_running, m_connected, m_waiting;
	uint32_t m_packetsSent;
	Ptr<WifiNetDevice> m_dev;
	TypeId m_tid;
	Ptr<ApWifiMac2> m_APMac;
	apClientManagero * m_clientManager, *m_WatchList;
	uint8_t m_chann; //!< Our channel
	typedef std::vector<InetSocketAddress> neighborList;
	neighborList m_otherAPs; //!< neihgbor APs
	neighborList::const_iterator m_cAddress;

	// RSSI threshold
	double m_rssi_thres;
	Time m_response_timeout, m_tcp_interval, m_startTime;
};

NS_LOG_COMPONENT_DEFINE("McvapApp");
McvapApp::McvapApp() :
		m_socket(0), m_sendEvent(), m_packetsSent(0) {
	//m_listen = 0;
	m_running = false;
	m_connected = 0;
	m_clientManager = 0;
	m_rssi_thres = global_rssi; //!< Default RSSI threshold
	m_WatchList = NULL;
	m_chann = 0;
	m_waiting = false;
}

McvapApp::~McvapApp() {
	m_socket = 0;
}

void McvapApp::Setup(TypeId protocol, Address ourAddr, uint8_t channel) {
	//m_peer = remoteDest;
	m_local = ourAddr;
	m_tid = protocol;
	m_connected = false;
	m_chann = channel;
	m_waiting = false;
	m_response_timeout = Seconds(1.5);
}

void McvapApp::AddNeighbor(InetSocketAddress addr) {
	m_otherAPs.push_back(addr);
}

void McvapApp::ChangeChannel(int channel) {
	if (!m_dev) m_dev = GetNode()->GetDevice(2)->GetObject<WifiNetDevice>();
	m_dev->GetPhy()->SetChannelNumber(channel);
}

void McvapApp::StartApplication(void) {
	m_running = true;
	std::ostringstream os;
	os << "Starting Application" << " channel " << (int) m_chann;
	NS_LOG_INFO(os.str());

	if (!InetSocketAddress::IsMatchingType(m_local)
			&& !PacketSocketAddress::IsMatchingType(m_local)) {
		NS_LOG_WARN("Local address is not matching type");
	}
	// starttime
	m_startTime = Simulator::Now();
	// start listening on AP_MSG_PORT
	StartListen();

	// Connect to MAC class of the node
	Ptr<WifiNetDevice> wifiAPDevice = GetNode()->GetDevice(2)->GetObject<
			WifiNetDevice>();
	if (!wifiAPDevice) {
		NS_FATAL_ERROR("WifiNetDevice not found");
	}
	Ptr<ApWifiMac2> m_mac = wifiAPDevice->GetMac()->GetObject<ApWifiMac2>();
	if (m_mac) setLowRssiCallBack(m_mac);
	else
	NS_FATAL_ERROR("Failed to retrieve ApWifiMac2 for the node");
}

void McvapApp::StopApplication(void) {
	NS_LOG_INFO("Stopping Application");
	m_running = false;

	if (m_sendEvent.IsRunning()) Simulator::Cancel(m_sendEvent);

	if (m_socket) m_socket->Close();

	while (!m_socketList.empty()) { //these are accepted sockets, close them
		Ptr<Socket> acceptedSocket = m_socketList.front();
		m_socketList.pop_front();
		acceptedSocket->Close();
	}
}

void McvapApp::StartListen() {
	if (!m_socket) {
		//m_socket->Close(); // needs to be closed before this
		m_socket = Socket::CreateSocket(GetNode(), m_tid);
		m_socket->Bind(InetSocketAddress(Ipv4Address::GetAny(), AP_MSG_PORT));
		m_socket->Listen();
		m_socket->SetRecvCallback(MakeCallback(&McvapApp::HandleRead, this));
		m_socket->SetAcceptCallback(
				MakeNullCallback<bool, Ptr<Socket>, const Address&>(),
				MakeCallback(&McvapApp::HandleAccept, this));
		m_socket->SetCloseCallbacks(
				MakeCallback(&McvapApp::HandlePeerClose, this),
				MakeCallback(&McvapApp::HandlePeerError, this));
	}
	NS_LOG_FUNCTION("lst");
}

void McvapApp::HandleAccept(Ptr<Socket> s, const Address& from) {
	std::ostringstream os;
	os << Simulator::Now().GetMilliSeconds() << "  Connection accepted from "
			<< InetSocketAddress::ConvertFrom(from).GetIpv4() << " port "
			<< InetSocketAddress::ConvertFrom(from).GetPort() << " to "
			<< InetSocketAddress::ConvertFrom(m_local).GetIpv4() << " port "
			<< InetSocketAddress::ConvertFrom(m_local).GetPort();
	NS_LOG_FUNCTION(os.str());
	s->SetRecvCallback(MakeCallback(&McvapApp::HandleRead, this));
	m_socketList.push_back(s);
}

void McvapApp::HandlePeerClose(Ptr<Socket> socket) {
	//NS_LOG_FUNCTION("Peer Closed Normally");
}

void McvapApp::HandlePeerError(Ptr<Socket> socket) {
	NS_LOG_FUNCTION("Peer closed abnormally");
}

void McvapApp::SendPacket(Ptr<Packet> packet) {
	NS_LOG_FUNCTION("Sending Packets");
	if (!m_connected) {
		NS_FATAL_ERROR("No connection, can not send packets");
		return;
	}
	m_socket->Send(packet);
	m_socket->SetDataSentCallback(MakeCallback(&McvapApp::DataSent, this));
	m_socket->Close();
}

void McvapApp::DataSent(Ptr<Socket> socket, uint32_t size) {
	NS_LOG_FUNCTION(this << "packet size = " << size);
	if (!m_waiting) {
		NS_LOG_FUNCTION("No waiting packet to be sent");
		StartListen();
		return;
	}
	m_cAddress++;
	while (InetSocketAddress::ConvertFrom(*m_cAddress).GetIpv4()
			== InetSocketAddress::ConvertFrom(m_local).GetIpv4())
		m_cAddress++;
	if (m_cAddress == m_otherAPs.end()) {
		m_waiting = false;
		NS_LOG_FUNCTION("Finished sending to neighbors");
		StartListen();
		return;
	}
	ScheduleTx(m_packet, *m_cAddress);
}

void McvapApp::ScheduleTx(Ptr<Packet> packet, Address dest) {
	std::ostringstream os;
	os << Simulator::Now().GetMilliSeconds() << " Called schedule "
			<< packet->ToString() << " addr = "
			<< InetSocketAddress::ConvertFrom(dest).GetIpv4() << " on port "
			<< InetSocketAddress::ConvertFrom(dest).GetPort();
	NS_LOG_FUNCTION(os.str());
	if (m_running) {
		//if(m_socket)
		//m_socket->Close();
		m_packet = packet;
		m_socket = Socket::CreateSocket(GetNode(), m_tid);
		m_socket->Bind(m_local);
		m_socket->Connect(dest);
		m_socket->ShutdownRecv();
		m_socket->SetConnectCallback(MakeCallback(&McvapApp::ConnectSucc, this),
				MakeCallback(&McvapApp::ConnectFail, this));
	} else {
		NS_LOG_WARN(Simulator::Now().GetMilliSeconds() << " Not running");
	}
}

void McvapApp::ConnectSucc(Ptr<Socket> socket) {
	NS_LOG_FUNCTION(
			Simulator::Now().GetMilliSeconds() << " Connection successfull");
	m_connected = true;
	m_sendEvent = Simulator::ScheduleNow(&McvapApp::SendPacket, this, m_packet);
}

void McvapApp::ConnectFail(Ptr<Socket> socket) {
	NS_LOG_WARN(Simulator::Now().GetMilliSeconds() << " Failed");
	m_connected = false;
}

void McvapApp::HandleRead(Ptr<Socket> socket) { //!< Messages from other APs
	Ptr<Packet> packet;
	Address from;
	while ((packet = socket->RecvFrom(from))) {
		if (packet->GetSize() == 0) {
			break; //EOF
		}
		int pSize = packet->GetSize();
		if (InetSocketAddress::IsMatchingType(from)) {
			std::ostringstream os;
			os << "At time " << Simulator::Now().GetSeconds()
					<< "s packet McvapApp received " << pSize << " bytes from "
					<< InetSocketAddress::ConvertFrom(from).GetIpv4()
					<< " port "
					<< InetSocketAddress::ConvertFrom(from).GetPort();
			NS_LOG_FUNCTION(os.str());
		}
		HandleResponse(packet, from);
	}
}

void McvapApp::ResetScanRequest(p_client c) {
	if (c) c->will_handoff = false;
}

void McvapApp::SendRequest(uint8_t code, p_client c) { //!< Includes scan response
	NS_LOG_FUNCTION(this);
	if (c == NULL) {
		NS_LOG_WARN("NULL pointer for client");
		return;
	}
	std::ostringstream os;
	uint8_t p[64];
	int length = 0, stationCount = m_clientManager->m_stations.size();
	p[length++] = code;
	c->m_macaddress.CopyTo(p + length);
	length += 6;
	uint32_t ip = InetSocketAddress::ConvertFrom(m_local).GetIpv4().Get();
	memcpy((p + length), &ip, 4);
	length += 4;
	double rssi, per;
	switch (code) {
	//!< if it is a scan request send to everyone otherwise send to given
	case AP_SCAN_REQUEST:
		//!< Packet: scanreq code + mac address + ip address + station BSSID + station channel
		c->m_bssid.CopyTo(p + length);
		length += 6;
		p[length++] = m_chann;
		os << Simulator::Now().GetMilliSeconds() << ": Requesting Monitor for "
				<< c->m_macaddress << " current channel " << (uint32_t) m_chann;
		break;
	case AP_SCAN_RESPONSE:
		//!< Packet: scanres code + mac address + ip address + Rssi + our Channel
		if (!m_WatchList) {
			NS_FATAL_ERROR("WatchList is NULL");
		}
		m_WatchList->deleteStation(c->m_macaddress);
		rssi = c->rssi;
		memcpy(p + length, &rssi, sizeof(double));
		length += sizeof(double);
		p[length++] = m_chann;
		memcpy(p + length, &per, sizeof(double)); //!< Add PER to scan Response
		length += sizeof(double);
		memcpy(p + length, &stationCount, sizeof(int)); //!< Add Number of stations to scan Response
		length += sizeof(int);

		os << Simulator::Now().GetMilliSeconds()
				<< ": Sending Monitor Response for " << c->m_macaddress
				<< " PER: " << per << ", N = " << stationCount
				<< " new channel " << (uint32_t) m_chann;
		break;
	case AP_STATION_MOVE:
		//!< Packet: stamove code + mac address + ip address + csaCount + beacon Interval
		if (c->new_channel == m_chann || c->new_channel == 0) {
			NS_LOG_INFO(Simulator::Now().GetMilliSeconds() << " No better AP found");
			c->will_handoff = false; //!< set handoff to false for further scanning
			return;
		}
		os << Simulator::Now().GetMilliSeconds() << ": Moving "
				<< c->m_macaddress << " to channel " << c->new_channel;
		p[length++] = (uint8_t) ++(c->csa_count); //!< AP will remove the station
		break;
	default:
		NS_FATAL_ERROR("False code requested: code = " << code);
	}
	// Test packet
	Ptr<Packet> pkt = Create<Packet>(p, length);
	if (m_otherAPs.size() < 1) {
		NS_LOG_WARN("No neighbors");
		return;
	}
	NS_LOG_INFO(os.str());

	if (code == AP_SCAN_REQUEST) { //!< Send to all neighbors and schedule
		m_cAddress = m_otherAPs.begin();
		while (InetSocketAddress::ConvertFrom(*m_cAddress).GetIpv4()
				== InetSocketAddress::ConvertFrom(m_local).GetIpv4())
			m_cAddress++; //!< Dont send packet to self
		m_waiting = true;
		ScheduleTx(pkt, *m_cAddress);
		//!< Schedule client move send
		Simulator::Schedule(m_response_timeout, &McvapApp::SendRequest, this,
		AP_STATION_MOVE, c);
		return;
	}
	InetSocketAddress dest = InetSocketAddress(c->m_apIpaddress, AP_MSG_PORT);
	ScheduleTx(pkt, dest);
}

void McvapApp::HandleResponse(Ptr<Packet> packet, Address from) {

	// getCode
	uint8_t code;
	uint8_t mac[6];
	uint32_t ip;
	uint8_t channel;
	packet->CopyData(&code, 1);
	packet->RemoveAtStart(1);
	packet->CopyData(mac, 6);
	packet->RemoveAtStart(6);
	packet->CopyData((uint8_t*) &ip, 4);
	packet->RemoveAtStart(4);

	char t[18];
	sprintf((char *) t, f_MACADDR, MACADDR(mac));
	const char macString[18] = { t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7],
			t[8], t[9], t[10], t[11], t[12], t[13], t[14], t[15], t[16], t[17] };
	Mac48Address macAddr(macString);
	p_client wc;
	Ipv4Address senderIp = Ipv4Address(ip);

	p_client c = NULL;
	uint8_t rssi[8], per[8];
	double rssi2, per2;
	int num, ourNum = m_clientManager->m_stations.size();
	if (sizeof(double) != 8)
	NS_FATAL_ERROR("sizeof double is not eight ");
	std::ostringstream os;
	uint8_t resp_chann; //!< Channel number of response msg

	switch (code) {
	case AP_SCAN_REQUEST: //!< add to watchlist and (dont)register "from" Address to that client
		os << Simulator::Now().GetMilliSeconds() << ": Scan Requested for :"
				<< macAddr << ", from:" << senderIp;
		packet->RemoveAtStart(6); // BSSID
		packet->CopyData(&channel, 1);
		wc = m_WatchList->addStation(macAddr);
		wc->new_channel = channel;
		wc->m_apIpaddress = senderIp;
		wc->rssi = 0;
		ChangeChannel(channel);
		m_changeChannEv = Simulator::Schedule(Time("50ms"),
				&McvapApp::ChangeChannel, this, m_chann);
		break;
	case AP_SCAN_RESPONSE: //!< look up for given mac from own stations and check for channel switch
		c = m_clientManager->searchStation(macAddr);
		if (!c) {
			NS_LOG_WARN("Scan response for unknown client " << macAddr);
			return;
		}
		packet->CopyData(rssi, 8);
		packet->RemoveAtStart(8);
		packet->CopyData(&resp_chann, 1);
		packet->RemoveAtStart(1);
		packet->CopyData(per, 8);
		packet->RemoveAtStart(8);
		packet->CopyData((uint8_t*) &num, 4);
		packet->RemoveAtStart(4);
		memcpy(&rssi2, rssi, 8);
		memcpy(&per2, per, 8);
		os << Simulator::Now().GetMilliSeconds()
				<< ": Scan Response with info :" << macAddr << ", ip:"
				<< senderIp << " PER:" << per2 << ", N = " << num
				<< "  comparing RSSI ours:" << c->rssi << " and response:"
				<< rssi2;
		/*
		 if (rssi2 > c->rssi && rssi2 > c->new_rssi) { //!< dont change the csa count (client move will handle it)
		 c->new_rssi = rssi2;
		 c->m_apIpaddress = senderIp;
		 c->new_channel = resp_chann;
		 }
		 */
		if (global_algo == 1) { // Smart algorithm
			if (rssi2 > (c->rssi + 5.0) && rssi2 > c->new_rssi) {
				c->new_rssi = rssi2;
				c->m_apIpaddress = senderIp;
				c->new_channel = resp_chann;
			}
		} else if (global_algo == 2) { // Decentralized algorithm
			if (W_TH(per2, num) > W_TH(c->per, ourNum)
					&& W_TH(per2, num) > W_TH(c->new_per, c->new_num)) {
				c->new_num = num;
				c->new_per = per2;
				c->new_rssi = rssi2;
				c->m_apIpaddress = senderIp;
				c->new_channel = resp_chann;
			}
		} else { // RSSI algorithm
			if (rssi2 > c->rssi && rssi2 > c->new_rssi) {
				c->new_rssi = rssi2;
				c->m_apIpaddress = senderIp;
				c->new_channel = resp_chann;
			}
		}
		/* */
		break;
	case AP_STATION_MOVE:
		os << Simulator::Now().GetMilliSeconds() << ": Station Move for :"
				<< macAddr << ", from:" << Ipv4Address(ip);
		//!< Add client to the associated list
		m_APMac->AddClient(macAddr);
		break;
	}
	NS_LOG_INFO(os.str());
}

void McvapApp::ProcessDataPacket(const WifiMacHeader * hdr, double rssi,
		double per) {
	/* BSSID, Source ADDR, Destionation ADDR*/
	std::ostringstream os;
	os << "packet addr1 " << hdr->GetAddr1() << " addr2 " << hdr->GetAddr2()
			<< " addr3 " << hdr->GetAddr3() << " rssi = " << rssi << ", per = "
			<< per;
	NS_LOG_LOGIC(os.str());

	Mac48Address bs = hdr->GetAddr1();
	Mac48Address sa = hdr->GetAddr2();
	static int watchCount = 0;
	//Mac48Address da = hdr->GetAddr3();

	if (!m_clientManager || !m_WatchList)
	NS_FATAL_ERROR("Either one of the Client Managers is NULL");

	p_client c = m_clientManager->searchStation(sa);
	p_client wc = m_WatchList->searchStation(sa);

	if (c != NULL && bs == m_APMac->GetAddress()) { //!< If it is our client (check the RSSI value)
		NS_LOG_LOGIC("Our client " << sa << ", rssi Thres = " << m_rssi_thres);
		c->rssi = rssi;
		c->per = per;
		if (rssi < m_rssi_thres && !c->will_handoff) {
			NS_LOG_FUNCTION(
					Simulator::Now().GetMilliSeconds() << " Rssi(" << rssi << ") dropped for client " << sa);
			if (Simulator::Now() < (m_startTime + Seconds(2.))) {
				NS_LOG_FUNCTION("Too early to handoff");
				return;
			}
			c->will_handoff = true;
			//!< send SCAN request
			SendRequest(AP_SCAN_REQUEST, c);
		}
	} else if (wc != NULL) { //!< watched client (update RSSI and PER)
		watchCount++;
		wc->rssi = (wc->rssi * (watchCount - 1) + rssi) / watchCount; //!< average
		wc->per = (wc->per * (watchCount - 1) + per) / watchCount; //!< average
		NS_LOG_FUNCTION(
				Simulator::Now().GetMilliSeconds() << " Got rssi for watched client: " << rssi);

		if (watchCount >= 2) { //!< Listen to 2 packets (channel switch back timeout should be longer for this to be more than 2)
			watchCount = 0;
			//!< Cancel change channel back event (do it Now) Send Scan Response
			if (m_changeChannEv.IsRunning()) {
				m_changeChannEv.Cancel();
				ChangeChannel(m_chann);
			}
			SendRequest(AP_SCAN_RESPONSE, wc); //!< Send scan response
		}
	}
}

void McvapApp::setLowRssiCallBack(Ptr<ApWifiMac2> m_mac) {
	m_APMac = m_mac;
	m_clientManager = m_APMac->m_clientManager;
	m_WatchList = m_APMac->m_watchListManager;
	for (stationList::iterator i = m_clientManager->m_stations.begin();
			i != m_clientManager->m_stations.end(); i++) {
		((p_client) *i)->m_apIpaddress =
				InetSocketAddress::ConvertFrom(m_local).GetIpv4();
		((p_client) *i)->channel = m_chann;
		((p_client) *i)->new_channel = m_chann;
	}
	m_APMac->setLowRSSICallback(
			MakeCallback(&McvapApp::ProcessDataPacket, this));
}

FlowMonitorHelper flowmon;
Ptr<FlowMonitor> monitor = NULL;
void startMon(Ipv4Address from, Ipv4Address to) {
	if (!monitor) return;
	monitor->CheckForLostPackets();

	Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier>(
			flowmon.GetClassifier());
	std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats();
	for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i =
			stats.begin(); i != stats.end(); ++i) {
		Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);
		if ((t.sourceAddress == from && t.destinationAddress == to)) {
			std::cout << Simulator::Now().GetMilliSeconds() << " Throughput: "
					<< i->second.rxBytes * 8.0
							/ (i->second.timeLastRxPacket.GetSeconds()
									- i->second.timeFirstTxPacket.GetSeconds())
							/ 1024 << " Kbps\n";
		}
	}
	Simulator::Schedule(Seconds(0.25), &startMon, from, to);
}

Ptr<WifiNetDevice> mwifiDev;
void LinkUp() {
	bool newAP = false;
	static bool stationIsUp = true;
	static int apCount = 0;
	static Mac48Address cBssid = Mac48Address();
	if (mwifiDev) {
		stationIsUp = mwifiDev->IsLinkUp();
		Ptr<WifiMac> mac = mwifiDev->GetMac();
		Ptr<RegularWifiMac> rmac = mac->GetObject<RegularWifiMac>();
		if (rmac && rmac->GetBssid() != cBssid) {
			cBssid = rmac->GetBssid();
			newAP = true;
			apCount++;
		}
	} else {
		stationIsUp = !stationIsUp;
	}
	Time t_now = Simulator::Now();
	double f_milli = 1.0e-6 * t_now.GetNanoSeconds();
	NS_LOG_FUNCTION(
			"station is " << ((stationIsUp) ? "UP" : "DOWN") << " with " << ((newAP) ? "new" : "old") << " AP at " << f_milli);
	if (newAP) {
		monitor->StopRightNow();
		monitor->StartRightNow();
		//startMon();
	}
}

void sinkReceive(const Ptr<const Packet> packet, const Address &address) {
	static Time revT = Simulator::Now();
	NS_LOG_LOGIC("Received " << packet->GetSize() << " from " << address);
	Time delta = (Simulator::Now() - revT);
	std::cout << delta.GetMicroSeconds() / 1000.0 << std::endl;
	revT = Simulator::Now();
}

double vX, vY;
void changeDirection(Ptr<ConstantVelocityMobilityModel> mob, Time next) {
	static uint32_t d_counter = 0;
	double d_vx[] = { 0, -vX, 0, vX };
	double d_vy[] = { vY, 0, -vY, 0 };
	mob->SetVelocity(Vector(d_vx[d_counter], d_vy[d_counter], 0));
	d_counter = (d_counter + 1) % 4;
	Simulator::Schedule(next, &changeDirection, mob, next);
}

void setOwnSSID(Ptr<WifiNetDevice> staNetDevice) {
	Ptr<StaWifiMac2> staMacLayer;
	Mac48Address staMacAddress;
	if (staNetDevice) staMacLayer = staNetDevice->GetMac()->GetObject<
			StaWifiMac2>();
	else NS_FATAL_ERROR("WifiNetDevice is NULL");
	if (staMacLayer) staMacAddress = staMacLayer->GetAddress();
	else NS_FATAL_ERROR("WifiMac is NULL");

	std::ostringstream os;
	os << "Client-" << staMacAddress;
	staMacLayer->SetSsid(Ssid(os.str()));
}

int main(int argc, char *argv[]) {

	uint32_t maxChannel = 11, nWifis = 4, nSta = 24;
	bool writeMobility = false, outputIAT = false, outputTHRPT = false;
	bool setCrowd = false, oConstRate = false, hundred = false;
	double velX = 1/*, velY = 1, */, rssi_th = RSSI_THRESHOLD, gridRatio = 1.0;
	double startApp = 0.5, endApp = 60.0, walkDistance = 15.0;
	double rMin = 5.0, rMax = 15.0;
	double staDensity[] = { 3.1 / 24, 5.1 / 24, 7.1 / 24, 9.1 / 24 };
	double staDensity2[] = { 6.01 / 24, 6.01 / 24, 6.01 / 24, 6.01 / 24 };

	int algoValue = 0;

	CommandLine cmd;
	cmd.AddValue("gridRatio", "Number of wifi networks", gridRatio);
	cmd.AddValue("nSta", "Number of total stations", nSta);
	cmd.AddValue("writeMobility", "Write mobility trace", writeMobility);
	cmd.AddValue("oConstRate", "Other station communicate at endlessly", oConstRate);
	cmd.AddValue("setCrowd", "change station density", setCrowd);
	cmd.AddValue("outputIAT", "Output IAT time values to stdout", outputIAT);
	cmd.AddValue("outputTHRPT", "Output throughput to stdout", outputTHRPT);
	cmd.AddValue("velX", "Horizontal velocity of the station", velX);
	cmd.AddValue("rssi_th", "Rssi Threshold for the APs", rssi_th);
	cmd.AddValue("startApp", "Start time for application", startApp);
	cmd.AddValue("endApp", "End time for application", endApp);
	cmd.AddValue("walkDistance", "Distance station walks before changing direction", walkDistance);
	cmd.AddValue("algoValue", "algorithm to use, 1=smart, 2=decentralized",
			algoValue);
	cmd.AddValue("rMin", "Closest radius", rMin);
	cmd.AddValue("rMax", "Farthest radius", rMax);
	cmd.AddValue("hundred", "true=100Kbps, false=500Kbps", hundred);
	cmd.Parse(argc, argv);

	vX = vY = velX;
	global_rssi = rssi_th;
	global_algo = algoValue;
	LogComponentEnable("McvapApp", LOG_LEVEL_INFO);
	//LogComponentEnable("PPBPApplication", LOG_LEVEL_FUNCTION);

	NodeContainer backboneNodes;
	NetDeviceContainer backboneDevices;
	std::vector<NetDeviceContainer> apDevices;
	std::vector<Ipv4InterfaceContainer> apInterfaces;

	InternetStackHelper stack;
	CsmaHelper csma;
	Ipv4AddressHelper ip;
	Ptr<CsmaChannel> csmaChannel = CreateObjectWithAttributes<CsmaChannel>(
			"DataRate", StringValue("100Mbps"), "Delay", StringValue("10us"));
	ip.SetBase("192.168.0.0", "255.255.255.0");

	backboneNodes.Create(nWifis);
	stack.Install(backboneNodes);
	backboneDevices = csma.Install(backboneNodes, csmaChannel);

	NodeContainer serverNode;
	Ipv4InterfaceContainer svrInterface;
	MobilityHelper mobility;
	ListPositionAllocator mLPA;
	serverNode.Create(1);
	stack.Install(serverNode);
	NetDeviceContainer serverDev = csma.Install(serverNode, csmaChannel);
	svrInterface = ip.Assign(serverDev);
	mLPA.Add(Vector(35 * gridRatio, -10 * gridRatio, 0));
	mobility.SetPositionAllocator(&mLPA);

	// setup the AP.
	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobility.Install(serverNode.Get(0));
	//double wifiX = 10.0, wifiY = -20.0, deltaX = 60, staHeight = 30;

	//!< Wifi default configuration
	std::string phyMode("DsssRate11Mbps");
	WifiHelper wifi = WifiHelper::Default();
	wifi.SetStandard(WIFI_PHY_STANDARD_80211b);

	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
	wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO); //!< Support radiotap capture
	YansWifiChannelHelper wifiChannel;
	//!< reference loss must be changed since 802.11b is operating at 2.4GHz
	wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
	wifiChannel.AddPropagationLoss("ns3::LogDistancePropagationLossModel",
			"Exponent", DoubleValue(3.0), //!< office path loss exponent
			"ReferenceLoss", DoubleValue(40.0459));
	wifiPhy.SetChannel(wifiChannel.Create());
	//!< Add a non-QoS upper mac, and disable rate control
	NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();
	wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager", "DataMode",
			StringValue(phyMode), "ControlMode", StringValue(phyMode));

	double apX[] = { 25, 0, 25, 50 };
	double apY[] = { 0, 25, 50, 25 };
	for (uint32_t i = 0; i < nWifis; ++i) {
		NetDeviceContainer apDev;
		Ipv4InterfaceContainer apInterface;
		MobilityHelper mobility;
		BridgeHelper bridge;
		NetDeviceContainer bridgeDev;
		ListPositionAllocator mLPA;

		mLPA.Add(Vector(apX[i] * gridRatio, apY[i] * gridRatio, 0));
		mobility.SetPositionAllocator(&mLPA);
		mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
		mobility.Install(backboneNodes.Get(i));
		/// setup the AP.
		wifiMac.SetType("ns3::ApWifiMac2"); //!< No SSID is set
		apDev = wifi.Install(wifiPhy, wifiMac, backboneNodes.Get(i));

		//!< Change channel of the next AP
		int FreqChannel = 1 + i * ((maxChannel + 1) / nWifis);
		apDev.Get(0)->GetObject<WifiNetDevice>()->GetPhy()->SetChannelNumber(
				FreqChannel);
		bridgeDev = bridge.Install(backboneNodes.Get(i),
				NetDeviceContainer(apDev, backboneDevices.Get(i)));

		//!< assign AP IP address to bridge, not wifi
		apInterface = ip.Assign(backboneDevices.Get(i)); //bridgeDev);
		apDevices.push_back(apDev);
		apInterfaces.push_back(apInterface);
	}

	//!< Setup load stations
	NodeContainer stas;
	NetDeviceContainer stasDev;
	ListPositionAllocator m_statLPA;
	Ipv4InterfaceContainer stasInterface;
	MobilityHelper statmobility;
	statmobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	stas.Create(nSta); //!< load stations
	stack.Install(stas);
	Ptr<StaWifiMac2> stasMacLayer;
	wifiMac.SetType("ns3::StaWifiMac2", "ActiveProbing", BooleanValue(true));
	stasDev = wifi.Install(wifiPhy, wifiMac, stas);

	NS_ASSERT_MSG(nSta > nWifis, "Station count is less than AP count");
	double * den;
	if(setCrowd) den = staDensity2;
	else den = staDensity;
	uint32_t apSTACount[] = { (uint32_t) (den[0] * nSta),
			(uint32_t) (den[1] * nSta), (uint32_t) (den[2] * nSta),
			(uint32_t) (den[3] * nSta) };
	Ptr<UniformRandomVariable> m_rand = CreateObject<UniformRandomVariable>();
	for (uint32_t k = 0, j = 0; k < nWifis; k++) { // for every AP
		uint32_t apChannelNum = 1 + k * ((maxChannel + 1) / nWifis);
		for (uint32_t i = 0; i < apSTACount[k]; i++) {
			//!< TODOS: Raduis depends on the number of STA
			double m_radius = m_rand->GetValue(rMin, rMax); // radius
		//	std::cout << "radius = " << m_radius << std::endl;
			double m_angle = m_rand->GetValue(-M_PI, M_PI); //angle
			Vector staVec((apX[k] + m_radius * cos(m_angle)) * gridRatio,
					(apY[k] + m_radius * sin(m_angle)) * gridRatio, 0);
			m_statLPA.Add(staVec);
			Ptr<WifiNetDevice> i_netDev = stasDev.Get(j++)->GetObject<
					WifiNetDevice>();
			setOwnSSID(i_netDev);
			i_netDev->GetPhy()->SetChannelNumber(apChannelNum);
		}
	}

	stasInterface = ip.Assign(stasDev);
	statmobility.SetPositionAllocator(&m_statLPA);
	statmobility.Install(stas);

	/// setup the observed STA (make sure all variables are unique)
	NodeContainer m_sta;
	NetDeviceContainer m_staDev;
	MobilityHelper m_mobilitySta;
	ListPositionAllocator m_LPAsta;
	Ipv4InterfaceContainer m_staInterface;
	m_sta.Create(1);
	stack.Install(m_sta);
	m_mobilitySta.SetMobilityModel("ns3::RandomWalk2dMobilityModel", "Bounds",
			RectangleValue(
					Rectangle(-10 * gridRatio, 60 * gridRatio, -10 * gridRatio,
							60 * gridRatio)), "Distance",
			DoubleValue(walkDistance * gridRatio), "Speed",
			StringValue("ns3::UniformRandomVariable[Min=2.0|Max=4.0]"));
	//"ns3::ConstantVelocityMobilityModel");
	//!< Place the station above the first AP
	Vector m_staVec(25 * gridRatio, 25 * gridRatio, 0);
	m_LPAsta.Add(m_staVec);
	m_mobilitySta.SetPositionAllocator(&m_LPAsta);
	m_mobilitySta.Install(m_sta);

	//Ptr<ConstantVelocityMobilityModel> p_Stamobility;
	//p_Stamobility = m_sta.Get(0)->GetObject<ConstantVelocityMobilityModel>();
	//Simulator::Schedule(Seconds(startApp), &changeDirection, p_Stamobility,
	//		Seconds(30 * gridRatio / velX));

	wifiMac.SetType("ns3::StaWifiMac2", "ActiveProbing", BooleanValue(true));
	m_staDev = wifi.Install(wifiPhy, wifiMac, m_sta);
	setOwnSSID(m_staDev.Get(0)->GetObject<WifiNetDevice>());

	int chanNums[] = {1, 4, 7, 10};
	int sChan = chanNums[m_rand->GetInteger(0, 3)];
	NS_LOG_INFO("initial STATION channel is " << sChan);
	m_staDev.Get(0)->GetObject<WifiNetDevice>()->GetPhy()->SetChannelNumber(sChan);
	m_staInterface = ip.Assign(m_staDev);

	//Ipv4GlobalRoutingHelper::PopulateRoutingTables();
	std::string otherStaProtocol = "ns3::UdpSocketFactory"; //"ns3::TcpSocketFactory";
	std::string StaServerProtocol = "ns3::UdpSocketFactory";

	/** Inter AP Messages */
	int localPort = 12329;
	for (unsigned int i = 0; i < nWifis; i++) {
		InetSocketAddress addr1 = InetSocketAddress(
				apInterfaces[i].GetAddress(0), localPort);
		Ptr<McvapApp> mcvapp = CreateObject<McvapApp>();
		for (uint32_t j = 0; j < apInterfaces.size(); j++)
			mcvapp->AddNeighbor(InetSocketAddress(apInterfaces[j].GetAddress(0),
			AP_MSG_PORT));
		int channel = 1 + i * ((maxChannel + 1) / nWifis);
		mcvapp->Setup(TcpSocketFactory::GetTypeId(), addr1, channel);
		backboneNodes.Get(i)->AddApplication(mcvapp); // added to AP1
		mcvapp->SetStartTime(Seconds(startApp));
		mcvapp->SetStopTime(Seconds(endApp));
	}

	//! other stations communication
	//UniformRandomVariable
	Address svrAddress;
	uint32_t otherStaPort = 9;
	ApplicationContainer ppbpApps;
	svrAddress = InetSocketAddress(svrInterface.GetAddress(0), otherStaPort);
	PPBPHelper ppbp = PPBPHelper(otherStaProtocol, svrAddress);
	ppbp.SetAttribute("BurstIntensity", StringValue("500Kbps"));
	ppbp.SetAttribute("MeanBurstArrivals",
			StringValue("ns3::LogNormalRandomVariable[Sigma=0.25]"));
	//for (uint32_t i = 0; i < nSta; i++) {
	ppbpApps = ppbp.Install(stas);
	//}
	ppbpApps.Start(Seconds(startApp));
	ppbpApps.Stop(Seconds(endApp));

	/** This is onOff application for other STAs
	 * 	uint32_t rand_size = m_rand->GetInteger(100, 1000);
	 DataRate rand_rate(m_rand->GetInteger(1E5, 1E6)); //!< 100kbps to 1Mbps
	 OnOffHelper onoff = OnOffHelper(otherStaProtocol, svrAddress);
	 onoff.SetAttribute("PacketSize", UintegerValue(rand_size));
	 if (oConstRate) onoff.SetConstantRate(rand_rate);
	 else {
	 onoff.SetAttribute("OnTime",  StringValue ("ns3::UniformRandomVariable[Min=0.5|Max=2.0]"));
	 onoff.SetAttribute("OffTime", StringValue ("ns3::UniformRandomVariable[Min=2.0|Max=5.0]"));
	 onoff.SetAttribute("DataRate", DataRateValue (rand_rate));
	 }
	 udpApps = onoff.Install(stas.Get(i));
	 *
	 * */

	ApplicationContainer udpApps;
	uint32_t StaServerPort = 50491;
	/** Station-Server comm start */
	// sender (station)
	svrAddress = InetSocketAddress(svrInterface.GetAddress(0), StaServerPort);
	OnOffHelper onoff = OnOffHelper(otherStaProtocol, svrAddress);
	if(hundred)
		onoff.SetConstantRate(DataRate("100Kb/s"));
	else
		onoff.SetConstantRate(DataRate("500Kb/s"));

	onoff.SetAttribute("PacketSize", UintegerValue(200));

	udpApps = onoff.Install(m_sta.Get(0)); // added to STA
	udpApps.Start(Seconds(startApp));
	udpApps.Stop(Seconds(endApp));

	// Receiver (Server)
	PacketSinkHelper svrSink(StaServerProtocol,
			InetSocketAddress(Ipv4Address::GetAny(), StaServerPort));
	PacketSinkHelper svrSink2(StaServerProtocol,
			InetSocketAddress(Ipv4Address::GetAny(), otherStaPort));
	udpApps = svrSink.Install(serverNode.Get(0)); // added to SERVER
	udpApps = svrSink2.Install(serverNode.Get(0)); // added to SERVER
	//!< Output Inter-Arrival-Time for packets for server node
	if (outputIAT) {
		serverNode.Get(0)->GetApplication(0)->TraceConnectWithoutContext("Rx",
				MakeCallback(&sinkReceive));
		//serverNode.Get(0)->GetApplication(1)->TraceConnectWithoutContext("Rx", MakeCallback(&sinkReceive));
	}

	udpApps.Start(Seconds(startApp));
	udpApps.Stop(Seconds(endApp));
	/** Station-server comm end */

	//! Calculate Throughput using Flowmonitor
	//monitor = flowmon.InstallAll();
	//!< Output throughtput for observed node
	if (outputTHRPT) startMon(m_staInterface.GetAddress(0),
			svrInterface.GetAddress(0));

	//wifiPhy.EnablePcap("wifi-ap1", apDevices[0], false);
	//wifiPhy.EnablePcap("wifi-ap2", apDevices[1], false);
	//wifiPhy.EnablePcap("wifi-ap3", apDevices[2], false);
	//wifiPhy.EnablePcap("wifi-ap4", apDevices[3], false);
	//wifiPhy.EnablePcap("wifi-sta", m_staDev, false);
	//csma.EnablePcap("csma-server", serverDev, false);
	//csma.EnablePcap("csmaAp", backboneDevices, false);

	Packet::EnablePrinting();

	if (writeMobility) {
		AsciiTraceHelper ascii;
		MobilityHelper::EnableAsciiAll(
				ascii.CreateFileStream("wifi-wired-bridging.mob"));
	}

	//GtkConfigStore config;
	//config.ConfigureAttributes();

	 /*
	 std::string animFile = "mcvapAnim.xml";
	 AnimationInterface anim(animFile);
	 anim.SetMaxPktsPerTraceFile(200000);
	 anim.UpdateNodeColor(m_sta.Get(0), 255, 255, 0);
	 anim.UpdateNodeColor(serverNode.Get(0), 160, 160, 160);
	 for (uint32_t i = 0; i < nSta; i++)
	 anim.UpdateNodeColor(stas.Get(i), 0, 255, 0);
	 */


	Simulator::Stop(Seconds(endApp + 1.0));
	Simulator::Run();
	Simulator::Destroy();
}
