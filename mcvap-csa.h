/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Ghada Badawy <gbadawy@gmail.com>
 */
#ifndef MCVAP_CSA_H
#define MCVAP_CSA_H

#include <stdint.h>
#include "ns3/buffer.h"
#include "ns3/attribute-helper.h"
#include "ns3/wifi-information-element.h"

namespace ns3 {

class MCVAPCSA: public WifiInformationElement
{
public:
   MCVAPCSA ();
  void SetCSAMode (uint8_t csaMode);
  void SetNewChannel (uint8_t newChannel);
  void SetCSCount(uint8_t csCount);
  void SetCSA(bool onoff);
  bool GetCSA();
  uint8_t GetCSAMode (void) const;
  uint8_t GetNewChannel (void) const;
  uint8_t GetCSCount (void) const;

  WifiInformationElementId ElementId () const;
  uint8_t GetInformationFieldSize () const;
  void SerializeInformationField (Buffer::Iterator start) const;
  uint8_t DeserializeInformationField (Buffer::Iterator start,
                                       uint8_t length);
  Buffer::Iterator Serialize (Buffer::Iterator start) const;
  uint16_t GetSerializedSize () const;

private:
  uint8_t m_csMode;
  uint8_t m_newChannel;
  uint8_t m_csCount;
  bool add_csa;
};

std::ostream &operator << (std::ostream &os, const MCVAPCSA &MCVAPCSA);
std::istream &operator >> (std::istream &is, MCVAPCSA &MCVAPCSA);

ATTRIBUTE_HELPER_HEADER (MCVAPCSA)

} // namespace ns3

#endif /* MCVAP_CSA_H */
